int algorithm = 2;
int walls_wrap = 0;
int sim_time = 0;  //Counts up when active.
int sim_steps = 0;
int sim_active = 0;

//Some potential 2D turmites:
//Turm1 is: {{{ 1,8,0},{ 0,2,0},},{{ 1,2,1},{ 1,4,0},};  //Good generic builder.
//Turm1 is: {{{ 1,8,1},{ 0,2,0},},{{ 0,1,0},{ 0,2,1},};  //Builds random "dirt" in a corner (NE); can flip for rooms with paths on an angle.
//Turm1 is: {{{ 1,1,1},{ 1,8,0},},{{ 0,1,0},{ 0,8,1},}; //Really cool tiled pattern.
//Turm1 is: {{{ 0,1,1},{ 0,4,1},},{{ 1,8,0},{ 1,2,1},}; //Awesome spiral pattern!


//For counting.
int last_ts = 0;

final int NumCells = 50;
int[][] cells = new int[NumCells][NumCells];

final int[][] colors = {
  {0x22, 0x8B, 0x22}, //Grass
  {0xDE, 0xB8, 0x87}, //Dirt
};
final int Grass = 0;
final int Dirt = 1;
final int WALL = 99;  //Special; represents the wall if walls_wrap is off.

final int CellSize = 10;
final int WallBuffer = 10;

final String[] algo_names = new String[] {
  "Random",
};

//For turmites, if we are using them.
int[][] turmites = new int[][] {
  {0,0,0,0},  //x,y,state,direction[0,1,2,3=N,E,S,W]
  {0,0,0,0},
  {0,0,0,0},
  {0,0,0,0},
};

//Constructed later (randomly)
int[][][] turm1= {{{1, 2, 1}, {1, 8, 1}}, {{1, 2, 1}, {0, 2, 0}}};


class MooreNeighborhood {
  int n;
  int s;
  int e;
  int w;
  int ne;
  int se;
  int sw;
  int nw;
  
  boolean contains(int value) {
    return   n==value  || s==value  || e==value  || w==value 
          || ne==value || se==value || nw==value || sw==value;
  }
}

class VonNeumannNeighborhood {
  int n;
  int s;
  int e;
  int w;
  int nn;
  int ss;
  int ee;
  int ww;
  
  boolean contains(int value, boolean extended) {
    boolean extend_res = false;
    if (extended) {
      extend_res = nn==value || ss==value || ww==value || ee==value;
    }
    return n==value  || s==value  || e==value  || w==value || extend_res;
  }
}

int get_cell(int x, int y) {
  //Adjust for wrap.
  if (walls_wrap==1) {
    int h = cells.length;
    if (y<0) { y = h-1+y; }
    if (y>=h) { y = y-h; }
    int w = cells[y].length;
    if (x<0) { x = w-1+x; }
    if (x>=w) { x = x-w; }
  }
  
  //Now apply walls.
  int h = cells.length;
  int w = cells[0].length;
  if (x<0 || x>=w || y<0 || y>=h) { 
    return WALL;
  } else {
    return cells[y][x];
  }
}

MooreNeighborhood get_moore_neighborhood(int x, int y) {
  MooreNeighborhood res = new MooreNeighborhood();
  res.n = get_cell(x,y-1);
  res.s = get_cell(x,y+1);
  res.w = get_cell(x-1,y);
  res.e = get_cell(x+1,y);
  res.ne = get_cell(x+1,y-1);
  res.se = get_cell(x+1,y+1);
  res.nw = get_cell(x-1,y-1);
  res.sw = get_cell(x-1,y+1);
  return res;
}

VonNeumannNeighborhood get_von_neumann_neighborhood(int x, int y) {
  VonNeumannNeighborhood res = new VonNeumannNeighborhood();
  res.n = get_cell(x,y-1);
  res.s = get_cell(x,y+1);
  res.w = get_cell(x-1,y);
  res.e = get_cell(x+1,y);
  res.nn = get_cell(x,y-2);
  res.ss = get_cell(x,y+2);
  res.ww = get_cell(x-2,y);
  res.ee = get_cell(x+2,y);
  return res;
}


void setup() {
  size(520, 620);
  stroke(255);
  frameRate(30);
  reset_turmites();
  rebuild_turmites();
}

void rebuild_turmites() {
  for (int[][] row : turm1) {
    for (int[] col : row) {
      col[0] = random(100)>50 ? 1 : 0;
      col[1] = int(random(100));
      if (col[1]>75) { col[1] = 1;}
      else if (col[1]>50) { col[1] = 2;}
      else if (col[1]>25) { col[1] = 4;}
      else { col[1] = 8;}
      col[2] = random(100)>50 ? 1 : 0;
    }
  }
  
  System.out.println("Turm1 is: {");
  for (int[][] row : turm1) {
    System.out.println("  {");
    for (int[] col : row) {
      System.out.println("    { " + col[0] + "," + col[1] + "," + col[2] + "},");
    }
    System.out.println("  },");
  }
}

void reset_turmites() {
  //Reset turmites.
  if (algorithm==2) {
    turmites[0][0] = NumCells/2;
    turmites[0][1] = NumCells/2;
    turmites[0][2] = 0;
    turmites[0][3] = 0;
  }
}

void keyPressed() {
  if (key == '1') {
    algorithm = (algorithm+1) % 10;
    reset_turmites();
  }
  if (key == '2') {
    walls_wrap = (walls_wrap+1) % 2;
  }
  if (key == 'p' || key == 'P') {
    last_ts = millis();
    sim_active = (sim_active+1) % 2;
  }
  if (key == 'r' || key == 'R') {
    clear_cells();
    sim_steps = 0;
    last_ts = millis();
    rebuild_turmites();
    reset_turmites();
  }
}

void clear_cells() {
  cells = new int[NumCells][NumCells];
}

int tick_cells_0(int x, int y) {
  return int(random(colors.length));
}

int tick_cells_1(int x, int y) {
  VonNeumannNeighborhood o = get_von_neumann_neighborhood(x,y);
  if ((o.w==Dirt || o.n==Dirt) && o.s!=Dirt) {
    return Dirt;
  }
  if (random(10)>9) {
    return Dirt;
  }
  return Grass;
}

//final int[][][] turm2= {{{1, 2, 1}, {1, 8, 1}}, {{1, 2, 1}, {0, 2, 0}}};
void tick_cells_2() {
  //Sample.
  int clr = cells[turmites[0][1]][turmites[0][0]];
  int state = turmites[0][2];
  int[] dec = turm1[state][clr];
  int dir = turmites[0][3];
  
  //React
  int resClr = dec[0];
  if (dec[1]==2) {
    dir = (dir+1)%4;
  } else if (dec[1]==4) {
    dir = (dir+2)%4;
  } else if (dec[1]==8) {
    dir = dir-1;
    if (dir<0) {
      dir = 3;
    }
  }
  state = dec[2];

  //Write
  cells[turmites[0][1]][turmites[0][0]] = resClr;
  
  //Move
  if (dir==0) {
    turmites[0][1] -= 1;
  } else if (dir==1) {
    turmites[0][0] += 1;
  } else if (dir==2) {
    turmites[0][1] += 1;
  } else if (dir==3) {
    turmites[0][0] -= 1;
  }
  if (turmites[0][0]<0) {
    turmites[0][0] = NumCells-1;
  } else if (turmites[0][0]>=NumCells) {
    turmites[0][0] = 0;
  }
  if (turmites[0][1]<0) {
    turmites[0][1] = NumCells-1;
  } else if (turmites[0][1]>=NumCells) {
    turmites[0][1] = 0;
  }
  
  //Turn, state
  turmites[0][3] = dir;
  turmites[0][2] = state;
}

void tick_cells() {
  if (algorithm == 2) {
    //Turmites.
    tick_cells_2();
  } else {
    //Traditional CAs
    int[][] new_cells = new int[NumCells][NumCells];
    for (int y=0; y<cells.length; y++) {
      for (int x=0; x<cells[y].length; x++) {
        switch(algorithm) {
          case 0:
            new_cells[y][x] = tick_cells_0(x, y);
            break;
          case 1:
            new_cells[y][x] = tick_cells_1(x, y);
            break;
          default:
            throw new RuntimeException("Unknown algorithm!");
        }
      }
    }
    cells = new_cells;
  }
}

void draw() { 
  //Update?
  if (sim_active==1) {
    //Update timing.
    int new_ts = millis();
    sim_time += (new_ts - last_ts);
    last_ts = new_ts;
    
    //Update cells
    int TimeDiff = 10;
    while (sim_time>TimeDiff) {
      tick_cells();
      sim_steps += 1;
      sim_time -= TimeDiff;
    }
  }
  
  //Clear
  background(0xFF);
  
  //Draw our text controls
  fill(0x00);
  textSize(18);
  text("Algorithm: " + algorithm, 10, 25);
  text("Wall Wrap: " + (walls_wrap==1?"Yes":"No"), 170, 25);
  fill(0x00, 0x66, 0x00);
  text("(Start)", 10, 75);
  fill(0x00);
  text("Ticks: " + sim_steps, 170, 75);

  //Draw our control keys.
  fill(0x99);
  textSize(13);
  text("1", 10, 40);
  text("2", 170, 40);
  text("P", 10, 90);
  text("R(eset)", 170, 90);

  
  //Line between text and C.A.
  fill(0x00);
  rect(0,100-1, width, 2);
  
  //Draw cells.
  for (int y=0; y<cells.length; y++) {
    for (int x=0; x<cells[y].length; x++) {
      int startX = x*CellSize+WallBuffer;
      int startY = y*CellSize+WallBuffer + 100;
      int[] clr = colors[cells[y][x]];
      fill(clr[0], clr[1], clr[2]);
      stroke(0x99);
      rect(startX, startY, CellSize, CellSize);
    }
  }
} 