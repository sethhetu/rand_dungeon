#############################################################
# Random dungeon script.
# This script (TODO: properties of the script, how it works, etc.)
#
# @author Seth N. Hetu
# @version 1.0 2015/08/09
# @license MIT (http://opensource.org/licenses/MIT)
# 
# Inspired by Dungeon Creation 6, version 0.14 12/01/21 RGSS3
#   (by: Saba Kan, translator: kirinelf)
#############################################################

module RandomDungeon
  # The fun and fast FNV1-a hash!
  # Modified from: https://github.com/jakedouglas/fnv-ruby/blob/master/LICENSE
  def self.fnv1a_128(data)
    hash = 144066263297769815596495629667062367629
    data.bytes.each do |byte|
      hash = hash ^ byte
      hash = (hash * 309485009821345068724781371) % (2 ** 128)
    end
    return hash
  end
  
  #TODO: Structure slightly better.
  @@my_rand = nil
  
  # Helper: sample (in any version of Ruby) with a given random sequence.
  def sample(arr)
    return arr[uniform(0,arr.length)]
  end
  
  # Helper: shuffle (in any version of Ruby) with a given random sequence.
  def shuffle(arr)
    return arr.sort_by{@@my_rand.rand}
  end

  # Helper: Pull a value from a triangular distribution.
  def triangle(low, guess, high)
    u = @@my_rand.rand
    a = low.to_f
    b = high.to_f
    c = guess.to_f
    fc = (c-a) / (b-a)
    return a + Math.sqrt(u*(b-a)*(c-a)) if u<fc
    return b - Math.sqrt((1-u)*(b-a)*(b-c))
  end

  # Helper: Pull a value from a uniform distribution.
  def uniform(low, high)
    raise "BAD ARGS #{low} , #{high}" if high-low<0
    return low if low==high
    return (@@my_rand.rand(high-low) + low).to_i
  end


  # A triangle!
  class Triangle
    attr_accessor :centerX
    attr_accessor :centerY
    attr_accessor :angle1
    attr_accessor :angle2
    attr_accessor :angle3

    def initialize(centerX, centerY, angle1, angle2, angle3)
      @centerX = centerX
      @centerY = centerY
      @angle1 = angle1
      @angle2 = angle2
      @angle3 = angle3
    end
  end


  # A rectangle that contains a set of rooms (which may spill over slightly outside its bounds).
  # RoomGroups are split using a BSP algorithm (like Dungeon Creation), but then can generate multiple rooms per Group.
  class RoomGroup
    #The rectangle. (lx,ly) -> (hx,hy), inclusive.
    attr_accessor :lx
    attr_accessor :ly
    attr_accessor :hx
    attr_accessor :hy

    #List of rooms
    attr_accessor :rooms

    attr_accessor :theme

    def initialize(lx, ly, hx, hy)
      self.lx = lx
      self.ly = ly
      self.hx = hx
      self.hy = hy
      @rooms = []

      #General theme of rooms in this group.
      @theme = nil #TODO: Theme types?
    end

    def width
      return hx-lx+1
    end

    def height
      return hy-ly+1
    end
  end


  # A room is the basic unit of dungeons. Rooms have a rectangular size,
  # and contain various doors (mapped as pink tiles) that connect to 
  # hallways and thus other rooms. A room's rectangular area can be 
  # chipped away or blocked, but the paths between doors will never be lost.
  # Rooms never overlap, and all rooms are connected (through teleporters, if
  # all else fails).
  class Room
    attr_accessor :lx
    attr_accessor :ly
    attr_accessor :hx
    attr_accessor :hy
    attr_accessor :goal

    def initialize(lx, ly, hx, hy)
      #The obvious properties of a room
      @lx = lx
      @ly = ly
      @hx = hx
      @hy = hy

      #These properties are never seen, but they influence room generation.

      #The dotted goal of this room (for default paths)
      @goal = nil 
    end

    def width
      return @hx-@lx+1
    end

    def height
      return @hy-@ly+1
    end
  end

  # Contains tile constants.
  class Tile
    #"Empty" tiles
    #TODO: Right now Room doubles as Empty. We should probably fix this once we've totally removed all references to 0.
    Empty = 0

    #A Room.
    Room = 0

    #Various RoomGroup background colors (for debug mode only)
    RmGrp1 = 1
    RmGrp2 = 2
    RmGrp3 = 3
    RmGrp4 = 4
    RmGrp5 = 5
    RmGrp6 = 6
	
	#Represents a wall and ceiling (walls appear above the northwall)
	RmWall = 7
	RmCeiling = 8

    #A pit which bridges cross over. 
    Pit = 42

    #A bridge. May cross pits or just empty area.
    Bridge = 96

    #A door into a Room. Will always be on the outer 1px boundary of the room.
    Door = 97

    #A tunnel between rooms. Typically does not cross pits, typically is straight-cut.
    Tunnel = 98
	
    #Represents intentional defects
    RoomDefect1 = 101
    RoomDefect2 = 102
	
	#Water defects
	WaterDefect1 = 103
	WaterDefect2 = 104

    #Use this to explicitly represent an unknown tile.
    Unknown = 999
  end

  #Contains "Item" (layer 1) constants
  class Item
    Empty = 0

    #Represents the default path through a room OR a teleporter. Must never be blocked.
    DefaultPath = 1

    #TEMP
    SpecialSpot1 = 1  #NOTE: We need this (as a DefaultPath) for some tiny rooms to get a starting location.

    #Where we start/end in a floor.
    StartLocation = 3
    StaircaseDown = 4

    #Various obstructions
    Monster = 5
    Treasure = 6
    NPC = 7
    NorthWall = 8 #Used to represent the top wall of the room
	
	Teleporter = 9
  end


  # Used to represent an idealized RPG Maker map.
  # Contains set of tiles (laid out as [x][y][layer]
  # that fit within the given (0,0) -> (width/height) (INCLUSIVE).
  # There are also teleporters between pairs of rooms.
  class DummyMap
    attr_reader :width
    attr_reader :height
    attr_reader :data
    attr_accessor :teleports

    attr_accessor :monsters
    attr_accessor :treasures
    attr_accessor :npcs
	attr_accessor :teleport_pos

    def initialize(width, height)
      @width = width
      @height = height
      @data = []
      @teleports = [] #(rm,rm) of places to teleport
      for x in (0..@width)
        @data.push([])
        for y in (0..@height)
          @data[x].push([])
          for z in (0..3-1)
            @data[x][y].push(0)
          end
        end
      end

      #Monster index as {[x,y]->type}, where type is "S,A,B,C"; S is "super special", A,B,C are different types. 
      #Can assume S>>A>B>C, in terms of count and difficulty (of course, you could also ignore this.).
      @monsters = {}

      #Treasure index as {[x,y]->[type,cat]}, where type="S,A,B,C"; S is "super special" (blue chest), A,B,C are different quality levels.
      #Can assume S>>A>B>C, in terms of count and quality (but you can also ignore this, but please don't ignore S).
      #Here, "cat" is the category: "A,D,R,I,S,M" for "Attack" (weapon/shield), "Defense" (armor/gloves/legs), "Relic" (accessories),
      #  "Item" (Potions/Consumables), "Skill" (Learn It), "Misc" (Crafting/Shop Summoning Items, Misc. Plot Items)
      @treasures = {}

      #NPC index as {[x,y]->[type]}, where type="S,N,T" for "Save", "(Generic) NPC", and "Terminal" (enter a name to seed the next floor).
      @npcs = {}
	  
	  #Teleporters {{x,y} => {x,y}}
	  @teleport_pos = {}
    end

    #Does the given tile match an expected tileId?
    # tileId can also be an array if tileIds.
    #Returns false if the tile is out of bounds
    def is_tile?(cX,cY,tileId)
      return false unless valid_tile? cX,cY
      if tileId.is_a?(Array)
        return tileId.include? @data[cX][cY][0]
      else
        return @data[cX][cY][0] == tileId
      end
    end

    #Does the given item match an expected itemId?
    #Returns false if the item is out of bounds
    def is_item?(cX,cY,itemId)
      return false unless valid_tile? cX,cY
      return @data[cX][cY][1] == itemId
    end


    #Does the given rect have at least one tile that matches the given Id?
    def has_tile?(rect,tileId)
      for y in rect.ly..rect.hy
        for x in rect.lx..rect.hx
          return true if is_tile? x,y,tileId
        end
      end
      return false
    end

    #Is this tile valid?
    def valid_tile?(cX,cY)
      return cX>=0 && cX<=@width && cY>=0 && cY<=@height
    end

  end

  # Class that encapsulates a massive ImageMagick command and splits it so the shell doesn't go crazy.
  class ImgMagickCommand
    #MaxCommands = 1000  #Just a guess, really.
    MaxCommands = 100  #Windows is even more stringent

    def initialize(fn, outname, width, height)
      @width = width
      @height = height
      @currLine = ""
      @currCmds = 0
      @firstLine = true
      @fname = fn
      @outname = outname
      @file = File.open(fn, "w")
      @file.write("#!/bin/bash\n")
    end

    def write(line)
      @currLine += line
      @currCmds += 1

      check_lines()
    end

    def run_command()
      check_lines(true)

      @file.close()
      `chmod a+x #{@fname}`
      `./#{@fname}`
    end

private
    def check_lines(force=false)
      #Even force won't help here
      return if @currLine.empty?

      #Break into a new line?
      if @currCmds>=MaxCommands || force
        if @firstLine
          @file.write("convert -size #{@width}x#{@height} xc:black  #{@currLine}   #{@outname}\n") 
          @firstLine = false
        else
          @file.write("convert #{@outname} -size #{@width}x#{@height}  #{@currLine} #{@outname}\n")
        end
        @currLine = ""
        @currCmds = 0
      end
    end
  end


  # Our Map base class, which contains tile IDs and has a printing function
  # (which requires ImageMagick and works on OS-X and Linux  --maybe Windows with Powershell?).
  # Contains a DummyMap as "map" for historical reasons.
  class MapBase 
    attr_reader :map

    def get_color(id)
      return "black"            if id==Tile::Room
      return "SpringGreen4"     if id==Tile::RmGrp1
      return "LightSkyBlue4"    if id==Tile::RmGrp2
      return "khaki1"           if id==Tile::RmGrp3
      return "tan2"             if id==Tile::RmGrp4
      return "PaleVioletRed1"   if id==Tile::RmGrp5
      return "violet"           if id==Tile::RmGrp6
      return "blue"             if id==Tile::Pit
      return "VioletRed3"       if id==Tile::Door
      return "firebrick2"       if id==Tile::Bridge
      return "MediumTurquoise"  if id==Tile::Tunnel
      return "OliveDrab2"       if id==Tile::RmWall
      return "coral3"           if id==Tile::RmCeiling
      return "OrangeRed"        #Unknown
    end

    # Print the map to console when we're done generating it.
    def pretty_print(filename)
      #Write our first command, which fills the image background.
      tileSz = 16
      file = ImgMagickCommand.new "input.sh", filename, @map.width*tileSz, @map.height*tileSz

      #Now, add each tile (one-by-one)
      for y in (0..@map.height)
        for x in (0..@map.width)
          clr = get_color(@map.data[x][y][0])
          clr = "khaki1" if clr=="RoyalBlue4"  #We'll do walls later.
          startX = x*tileSz
          startY = y*tileSz
          file.write("  -fill #{clr} -stroke gray  -draw \"rectangle #{startX},#{startY},#{startX+tileSz},#{startY+tileSz}\"  ")
        end
      end

      #Draw some annotations
      for y in (0..@map.height)
        for x in (0..@map.width)
          startX = x*tileSz
          startY = y*tileSz

          #Draw default spots
          if @map.is_item? x , y , Item::DefaultPath
            center = [startX+tileSz/2, startY+tileSz/2]
            file.write("  -fill VioletRed3 -stroke black  -draw \"circle #{center[0]},#{center[1]},#{center[0]-tileSz/5},#{center[1]-tileSz/5}\"  ")
          elsif @map.is_item? x , y , Item::SpecialSpot1 #TEMP
            center = [startX+tileSz/2, startY+tileSz/2]
            file.write("  -fill RoyalBlue2 -stroke black  -draw \"circle #{center[0]},#{center[1]},#{center[0]-tileSz/5},#{center[1]-tileSz/5}\"  ")
          end

          #Start location
          if @map.is_item? x , y , Item::StartLocation
            myX = startX + tileSz/5
            myY = startY + tileSz/5
            file.write("  -fill LightGoldenrod1 -stroke black  -draw \"rectangle #{myX},#{myY},#{myX+3*tileSz/5},#{myY+3*tileSz/5}\"  ")
            file.write("  -fill white -stroke gray  -font \"Liberation-Sans-Bold\" -pointsize 22  -draw \"text #{myX},#{myY} 'St'\"")
          end

          #Staircase down.
          if @map.is_item? x , y , Item::StaircaseDown
            myX = startX + tileSz/5
            myY = startY + tileSz/5
            file.write("  -fill sienna3 -stroke black  -draw \"rectangle #{myX},#{myY},#{myX+3*tileSz/5},#{myY+3*tileSz/5}\"  ")
            file.write("  -fill white -stroke gray  -font 'Liberation-Sans-Bold' -pointsize 22  -draw \"text #{myX},#{myY} 'Ed'\"")
          end

          #Monsters
          if @map.is_item? x , y , Item::Monster
            monst_value = @map.monsters[[x,y]]
            myX = startX + tileSz/5
            myY = startY + tileSz/5
            file.write("  -fill DarkOrchid4 -stroke black  -draw \"rectangle #{myX},#{myY},#{myX+3*tileSz/5},#{myY+3*tileSz/5}\"  ")
            file.write("  -fill white -stroke gray  -font \"Liberation-Sans-Bold\" -pointsize 22  -draw \"text #{myX},#{myY} 'm:#{monst_value}'\"")
          end

          #Treasures
          if @map.is_item? x , y , Item::Treasure
            treas_values = @map.treasures[[x,y]]
            myX = startX + tileSz/5
            myY = startY + tileSz/5
            file.write("  -fill SpringGreen1 -stroke black  -draw \"rectangle #{myX},#{myY},#{myX+3*tileSz/5},#{myY+3*tileSz/5}\"  ")
            file.write("  -fill white -stroke gray  -font \"Liberation-Sans-Bold\" -pointsize 22  -draw \"text #{myX},#{myY} 't:#{treas_values[0]}:#{treas_values[1]}'\"")
          end

          #The north wall of a room
          if @map.is_item? x , y , Item::NorthWall
            endX = startX + tileSz
            endY = startY + tileSz
            file.write("  -stroke white  -draw \"line #{startX},#{startY},#{endX},#{endY}\"  ")
            file.write("  -stroke white  -draw \"line #{endX},#{startY},#{startX},#{endY}\"  ")
            file.write("  -stroke white  -draw \"line #{startX},#{startY},#{endX},#{startY}\"  ")
            file.write("  -stroke white  -draw \"line #{startX},#{endY},#{endX},#{endY}\"  ")
            file.write("  -stroke white  -draw \"line #{startX},#{startY},#{startX},#{endY}\"  ")
            file.write("  -stroke white  -draw \"line #{endX},#{startY},#{endX},#{endY}\"  ")
          end
        end
      end

      #Draw teleports
      letter = 'A'.ord
      for tel in @map.teleports
        for rm in tel
          center = [rm.lx+(rm.hx-rm.lx)/2,rm.ly+(rm.hy-rm.ly)/2]
          file.write("  -fill skyblue -stroke black  -draw 'circle #{center[0]*tileSz},#{center[1]*tileSz},#{center[0]*tileSz-3*tileSz/4},#{center[1]*tileSz-3*tileSz/4}'  ")
          file.write("  -fill blue -stroke white  -font 'Liberation-Sans-Bold' -pointsize 24  -draw \"text #{center[0]*tileSz+5*tileSz/4},#{center[1]*tileSz+tileSz/2} '#{letter.chr}'\"")
        end
        letter += 1
      end

      #TEMP: Draw lines from the center of a room to the upper-left tile of the rooms it directly connects to
      line_colors = ["orchid2","MediumPurple3","MediumSpringGreen","OliveDrab2","LightGoldenrod3","burlywood"]
      id = 0
      @room_paths.each{|rm, rooms|
        fromPt = [(rm.lx+rm.width/2)*tileSz, (rm.ly+rm.height/2)*tileSz]
        clr = line_colors[id]
        id = (id+1)%line_colors.length
        for rm2 in rooms
          toPt = [rm2.lx*tileSz+tileSz/2, rm2.ly*tileSz+tileSz/2]
          file.write("  -stroke #{clr} -draw \"line #{fromPt[0]},#{fromPt[1]} #{toPt[0]},#{toPt[1]}\"")
        end
      }


      #Run it
      file.run_command()
    end
  end

  
  #HACK: Running out of time
	class Itm 
	  Potion    = 21
	  Potion_MP = 22
	  HiPotion    = 23
	  HiPotion_MP = 24
	  MegaPotion    = 25
	  MegaPotion_MP = 26
	  TrumpTonic = 27
	end
	
	class Wpn 
	  PizFrk1 = 2
	  PizFrk2 = 3
	  PizFrk3 = 4
	  PizFrk_MAX = 5 #s-rank
	  
	  Prize1 = 26
	  Prize2 = 27
	  Prize3 = 28
	  Prize_MAX = 29 #s-rank
	  
	  Star1 = 51
	  Star2 = 52
	  Star3 = 53
	  Star_MAX = 54 #s-rank
	end
	
	#TODO: Re-using Weapons variable names (lazy)
	class Hair 
	  PizFrk1 = 11
	  PizFrk2 = 12
	  PizFrk3 = 13
	  PizFrk_MAX = 14 #s-rank
	  
	  Prize1 = 35
	  Prize2 = 36
	  Prize3 = 37
	  Prize_MAX = 38 #s-rank
	  
	  Star1 = 60
	  Star2 = 61
	  Star3 = 62
	  Star_MAX = 63 #s-rank
	end
	class Armr 
	  PizFrk1 = 83
	  PizFrk2 = 84
	  PizFrk3 = 85
	  PizFrk_MAX = 86 #s-rank
	  
	  Prize1 = 106
	  Prize2 = 107
	  Prize3 = 108
	  Prize_MAX = 109 #s-rank
	  
	  Star1 = 128
	  Star2 = 129
	  Star3 = 130
	  Star_MAX = 131 #s-rank
	end
  
  

  # This class contains our main algorithm. Right now, it is a huge, multi-line mess; perhaps we'll clean it up some time?
  # Once done, you can access .map.data to get the list of tiles. Additional metadata is available in .rect_list and .room_paths,
  #   as well as some additional stuff in .map. 
  # Note: You may wish to center .map.data when you re-write its tiles into an actual game map.
  class Fancy_Map < MapBase
    include RandomDungeon
	
	attr_accessor :room_groups

    #See main() for examples on what these should be.
    def initialize(ideal_room, ideal_dungeon, max_dungeon)
      @ideal_room = ideal_room
      @ideal_dungeon = ideal_dungeon
      @max_dungeon = max_dungeon
    end

    #Create and return a RoomGroup
    def add_room_group(lx, ly, hx, hy)
      #puts "New Room Group: #{lx},#{ly} => #{hx},#{hy}"
      rect = RoomGroup.new(lx, ly, hx, hy)
      @room_groups.push rect
      return rect
    end

    #Create and return a Room
    def add_room(rect, lx, ly, hx, hy)
      #puts "New Room: #{lx},#{ly} => #{hx},#{hy}"
      room = Room.new(lx, ly, hx, hy)
      rect.rooms.push room
      return room
    end

    #Calling this creates the map.
    #Expect this function to take at least a few seconds.
    def setup()
      # Our random number generator
      my_seed = Random.new_seed

      #TEMP! This is how we reuse rooms (128bit seed)
      #my_seed = 142566647068779352962609858286854623109
      #my_seed = 153536206027884179716824483476745587858
      #my_seed = 185099845251311089964509246376062821152
      #my_seed = 153855965450326559851471847751342888208
      #my_seed = 295486164795171787272099457114751760533
      #my_seed = 245381410384138912424006965932108389673
      #my_seed = 78620521358787643597847623520015895739
      #my_seed = 41200444309730049326665621703812249120
      #my_seed = 117963007884216613674426606094370097164  #Generates "Placement array is empty"
      #my_seed = 241831041594439730901026712510746350442
      #my_seed = 84371133846617162878005511617093927653 #Interesting NorthWall property w.r.t. bridges
	  #my_seed =  60699724487423905840589845722405635558 #Bad roomgen
      #my_seed = 66647094361014334097918921726719880831 #Treasure!
	  #my_seed = 52419232325658577620795709970742538360

	  
	  #HACKKKKKK
	  nm = $game_actors[15].name
	  if !nm.empty?
	    puts "HACKING SEED: #{nm}"
		my_seed = RandomDungeon.fnv1a_128(nm)
		$game_actors[15].name = ""
	  end

      #my_seed = 23277697969034818600997402231349693686   #Potential error.
      #my_seed = 2056298881335398696088325822968563301 #North wall with ROOM above
      #my_seed = 63409165314683978309073297516989140476  #Cool potential hideaway for a bonus treasure (near the water)
      #my_seed = 315114078945425005814192906634760048125  #"Can't draw default path"
      #my_seed = 17654778119122359382275780393028273543   #Crash; nil error.
	  
	  
	  #TO TEST:
	  #252867350689644792030065751808854692863    #room extended too far.
	  #my_seed = 95216096654946102089293242370839945625 #teleporter!
	  

      @@my_rand = Random.new my_seed
      puts "Generating room with random seed: #{@@my_rand.seed}"
	
      @map = nil #.data = [x,y,HEIGHT], with 0 as the base layer. Also contains .teleports, which are [rm,rm] pairs of connectivity.
      @room_groups = nil #list of RoomGroups, each of which contains 1+ Rooms
      @room_paths = nil #room->[room,..] connectors (including teleports). Computed AFTER phase 1, to be super-sure we've got everything.
      @connections = nil #Don't rely on this for anything; used internally.

      #Phase 1: Generate the rects, rooms, and connectors. Rooms are square at this point.
      generate_rooms()
      generate_connections()
      cleanup_rooms()

      #Phase 2: Vary the rooms through different means. May change room shape, but connectivity won't be altered.
      #         We also set some properties of rooms here (e.g., treasure affinity).
      compute_paths()
      define_walking_paths()
      walk_and_proc_rooms()
      cleanup_tiles()
      #TODO: 

      #Phase 3: Populate the rooms with treasures and monsters.
      #TODO:

      #Phase 4: (This MUST be last) --populate layer 2 with some coloring "hints" (like dirt, etc.)
      #TODO: Maybe better in the engine?

      #In case we've scrolled the output offscreen.
      puts "Generated room with random seed: #{@@my_rand.seed}"
    end


    #Returns the [monster, treasure, npc, nothing] tokens for the current floor. 
    #TODO: Based on level descended so far; possibly on "themes" too. (my_grp_id emulates themes for now)
    def get_action_items(my_grp_id)
      return [
        uniform(100-my_grp_id*15, 100-my_grp_id*10),  #Monsters. More common in early groups
        uniform( 50+my_grp_id*4,   50+my_grp_id*6),   #Treasures. More common in later groups
        0,   #NPCs. For now, nothing
        20   #Nothing. Constant.
      ]
    end

    #Return the number of monsters for a given action. 
    #TODO: Based on level descended so far; possibly on "themes" too. (my_grp_id emulates themes for now)
    def get_monster_count(my_grp_id)
      return uniform(0,1) + uniform(my_grp_id/2,3*my_grp_id/4)
    end

    #Return the monster's value ("S", "A", "B", "C") for a given monster.
    #TODO: Based on level descended so far; possibly on "themes" too. (my_grp_id emulates themes for now)
    def get_monster_value(my_grp_id)
      #Kind of repurposing this here...
      return select_action(
        [1+10*my_grp_id, 10+4*my_grp_id, 50-2*my_grp_id, 100-10*my_grp_id],
        ["S", "A", "B", "C"]
      )
    end

    #Return the number of treasures for a given action. 
    #TODO: Based on level descended so far; possibly on "themes" too. (my_grp_id emulates themes for now)
    def get_treasure_count(my_grp_id)
      return uniform(0,1) + uniform(my_grp_id/3,2*my_grp_id/3)
    end

	#HACKKKKKK getting desperate.
	@@treas_grp_hack = {
	  1 => [1 , 10 , 20 , 100-31],
	  2 => [2 , 15 , 25 , 100-42],
	  3 => [3 , 20 , 30 , 100-53],
	  4 => [4 , 25 , 35 , 100-64],
	  5 => [10 , 30 , 40 , 100-80],
	}
	
	
    #Return the treasure's values ("S", "A", "B", "C") and (A,D,R,I,S,M) for a given treasure.
    #TODO: Based on level descended so far; possibly on "themes" too. (my_grp_id emulates themes for now)
    def get_treasure_values(my_grp_id)	
      #Kind of repurposing this here...
	  #NOTE: We hack a bit and let "S" be "Items" below a certain floor and "Hair" otherwise. (I promise this made more sense when I wrote it.)
	  probs = [1,1,1,1,99]
	  probs = @@treas_grp_hack[my_grp_id] if @@treas_grp_hack.has_key? my_grp_id
      return [
        select_action(
		  probs,
          #[1+10*my_grp_id, 10+4*my_grp_id, 50-2*my_grp_id, 100-10*my_grp_id],
          ["S", "A", "B", "C"]
        ),
        select_action(
          [10,  20,  20,  10,  20], #These will actually vary by level/theme.
          ["I", "A", "D", "R", "S"]  #Misc is typically set manually, so we don't include it here.
        )
      ]
    end


    #Check the current placement array and make sure we have at least one spot for this mode (action type)
    #TODO: More ordering types.
    def check_and_set_placement(placement, action, rm)
      #Reset on new actions.
      placement[1] = [] unless placement[0]==action
      return true unless placement[1].empty?
      placement[0]=action

      #Need to rebuild. We have several possible patterns, from "random" to structured.
      type = ["R"]
      if rm.hx-rm.lx>=4 && rm.hy-rm.ly>=4 #Need slightly larger rooms for blocks
        type.push "B"
      end
      type = sample(type)
      if type=="B"
        #Block-based placement; try placing quadrants of 2-4 blocks
        opts = shuffle([[0,0],[1,0],[0,1],[1,1]])
        for i in 1..uniform(0,2)
          opts.shift
        end

        for x in 1..50 #Try to place a block set 50 times
          placement[1] = [] #Start over each time.
          new_pt = [uniform(rm.lx+2,rm.hx-2),uniform(rm.ly+2,rm.hy-2)]
          for opt in opts
            new_item = [new_pt[0]+opt[0],new_pt[1]+opt[1]]
            break unless item_can_place(placement[1], new_item, rm)
            placement[1].push new_item
          end
          if placement[1].length == opts.length
            break
          end
          puts "SET; can't place blocks" if x==50 #Definitely an error.
        end
      end

      if type=="R" || placement[1].empty? #Random is a good fallback.
        #Random placement
        for i in 1..uniform(1,4)
          for x in 1..50 #Try to place a point 50 times
            new_pt = [uniform(rm.lx+1,rm.hx-1),uniform(rm.ly+1,rm.hy-1)]
            if item_can_place(placement[1], new_pt, rm)
              placement[1].push new_pt
              break
            end
            puts "SET; can't place random" if x==50 #Not strictly an error as long as we get at least one.
          end
        end
      end

      if placement[1].empty?
        puts "ERROR: Placement array is empty; skipping item!"
        return false
      end
      return true
    end


    #Can we place an item into the given set of items?
    #We allow items that have an empty Moore neighborhood, since this
    #  guarantees the player can walk around them.
    #NOTE: The existing items can be next to the new item, as long as they are not directly on top.
    def item_can_place(items, new_item, rm)
      #Check existing items
      for item in new_item
        return false if item[0]==new_item[0] && item[1]==new_item[1]
      end

      #Check if this tile is on a bad spot
      for blacklisted in [Item::DefaultPath, Item::SpecialSpot1, Item::StartLocation, Item::StaircaseDown]
        return false if @map.is_item?(new_item[0], new_item[1], blacklisted)
      end

      #Check if nearby (Moore) tiles are bad.
      for i in 1..9
        next if i==5
        tile = [new_item[0], new_item[1]]
        tile[0] += 1 if [3,6,9].include? i
        tile[0] -= 1 if [1,4,7].include? i
        tile[1] += 1 if [7,8,9].include? i
        tile[1] -= 1 if [1,2,3].include? i
        next unless tile[0]>=rm.lx && tile[0]<=rm.hx && tile[1]>=rm.ly && tile[1]<=rm.hy
        for blacklisted in [Item::Monster, Item::Treasure, Item::NPC, Item::NorthWall, Item::Teleporter]
          return false if @map.is_item?(tile[0], tile[1], blacklisted)
        end
      end
      return true
    end



    #Major Phase 2: After this phase, we will have a list of room->[room,...] paths computed from 
    #               the actual tile values, and we will have mutated the rooms through various means.
    def compute_paths()
      @room_paths = {}

      #Initialize the room_paths array.
      @room_groups.each {|grp|
        grp.rooms.each {|rm|
          @room_paths[rm] = []
        }
      }

      #Set up teleporter connections 
      for tel in @map.teleports
        @room_paths[tel[0]].push tel[1] unless @room_paths[tel[0]].include? tel[1]
        @room_paths[tel[1]].push tel[0] unless @room_paths[tel[1]].include? tel[0]
      end

      #Now, build outwards.
      visited_tiles = {}  #(x,y)->1; we avoid exploring these paths, since they have already been considered.
      @room_groups.each {|grp|
        grp.rooms.each {|rm|
          #We're building a list of directly connected rooms, so 
          #first we need a list of all the pink tiles.
          start_doors = [] #(x,y)
          for x in rm.lx..rm.hx
            for y in rm.ly..rm.hy
              next unless @map.is_tile? x, y, Tile::Door
              start_doors.push [x,y] 
            end
          end

          #For each start door...
          start_doors.each{|stTile|
            goto_tiles = [stTile] #(x,y)

            #For each tile, scan out (on red/aqua tiles) until we find a pink tile.
            other_rooms = [rm] #All rooms this one connects to, including itself.
            while !goto_tiles.empty?
              tile = goto_tiles.shift
              visited_tiles[tile] = 1
              for i in 1..4
                new_tile = [tile[0], tile[1]]
                new_tile[0] += 1 if i==1
                new_tile[0] -= 1 if i==2
                new_tile[1] += 1 if i==3
                new_tile[1] -= 1 if i==4
                next unless @map.valid_tile? new_tile[0], new_tile[1]

                if @map.is_tile? new_tile[0], new_tile[1], Tile::Door
                  next if visited_tiles.include? new_tile
                  visited_tiles[new_tile] = 1
                  #Special case: we found a new room (ALWAYS check this, even if we somehow already visited it)
                  @room_groups.each {|otherGrp|
                    otherGrp.rooms.each {|otherRm|
                      next if other_rooms.include? otherRm
                      next unless new_tile[0]>=otherRm.lx && new_tile[0]<=otherRm.hx && new_tile[1]>=otherRm.ly && new_tile[1]<=otherRm.hy
                      other_rooms.push otherRm
                    }
                  }
                elsif @map.is_tile? new_tile[0], new_tile[1], [Tile::Bridge,Tile::Tunnel]
                  #Normal case: keep iterating
                  next if visited_tiles.include? new_tile
                  visited_tiles[new_tile] = 1
                  goto_tiles.push new_tile
                end
              end
            end

            #Now that we've traced this door to every other room, link each of those rooms to each other.
            # (We link from/to every pair of rooms because every room can reach each other through this one door.)
            for rm1 in other_rooms
              for rm2 in other_rooms
                @room_paths[rm1].push rm2 unless @room_paths[rm1].include? rm2
                @room_paths[rm2].push rm1 unless @room_paths[rm2].include? rm1
              end
            end

          }
        }
      }
    end


    #Mark an "obvious" path through each room, so that we can make sure not to erode the room away past that 
    #  point, or place treasures in a way that will block any of the doors.
    #(NOTE: We assume that teleporters will be placed somewhere on the obvious path; if this TRULY blocks a room 
    #       off, the player can simply teleport back to the room to walk past it).
    def define_walking_paths()
      @room_groups.each {|grp|
        grp.rooms.each {|rm|
          #We're building a list of directly connected rooms, so 
          #first we need a list of all the pink tiles. Track x,y max/min too.
          start_doors = [] #(x,y)
          x_rng = nil
          y_rng = nil
          for x in rm.lx..rm.hx
            for y in rm.ly..rm.hy
              next unless @map.is_tile? x, y, Tile::Door
              start_doors.push [x,y] 
              x_rng = [x,x] if x_rng.nil?
              y_rng = [y,y] if y_rng.nil?
              x_rng[0] = [x_rng[0],x].min
              x_rng[1] = [x_rng[1],x].max
              y_rng[0] = [y_rng[0],y].min
              y_rng[1] = [y_rng[1],y].max
            end
          end

          #In the VERY RARE case that this room is completely empty (which can only happen for teleporters),
          # spot a few random tiles and leave.
          if x_rng.nil?
            puts "Randomly random room is random."
            for i in 1..10
              x = uniform(rm.lx+1, rm.hx-1)
              y = uniform(rm.ly+1, rm.hy-1)

              #Rooms need a goal, so set the first point as the goal and build all points towards it. 
              if rm.goal.nil?
                rm.goal = [x,y]
              else
                walk_to_default(rm, [x,y], rm.goal)
              end
            end
            next
          end

          #In the not-so-rare case that this is a dead-end room, modify the potential range
          #to include more of the center of the room.
          if x_rng[0]==x_rng[1]
            x_rng[0] = rm.lx + rm.width/4
            x_rng[1] = rm.lx + 3*rm.width/4
          end
          if y_rng[0]==y_rng[1]
            y_rng[0] = rm.ly + rm.height/4
            y_rng[1] = rm.ly + 3*rm.height/4
          end

          #Finally, make sure we never include the absolute outer edge.
          x_rng[0] = [x_rng[0],rm.lx+1].max
          x_rng[1] = [x_rng[1],rm.hx-1].min
          y_rng[0] = [y_rng[0],rm.ly+1].max
          y_rng[1] = [y_rng[1],rm.hy-1].min

          #Next, we pick a random point within the min/max range of each door.
          rm.goal = [uniform(x_rng[0],x_rng[1]),uniform(y_rng[0],y_rng[1])]
          @map.data[rm.goal[0]][rm.goal[1]][1] = Item::DefaultPath

          #Each door has to build to this point, but it can stop if it finds another DefaultPath point
          #(since we know this will eventually hook up with the main path).
          start_doors.each{|tile|
            walk_to_default(rm, tile, rm.goal)
          }

          #TEMP: So we know where to go...
          @map.data[rm.goal[0]][rm.goal[1]][1] = Item::SpecialSpot1
        }
      }
    end
	
	
	#HACKING: running out of time.
	
	#floor->[common,rare,super-rare]
	@@items_per_floor = {  
	  1 => [Itm::Potion , Itm::Potion_MP , Itm::TrumpTonic],
	  2 => [Itm::Potion , Itm::Potion_MP , Itm::TrumpTonic],
	  3 => [Itm::Potion , Itm::HiPotion , Itm::TrumpTonic],
	  4 => [Itm::Potion , Itm::HiPotion , Itm::TrumpTonic],
	  5 => [Itm::Potion , Itm::HiPotion_MP , Itm::TrumpTonic],
	  6 => [Itm::HiPotion , Itm::HiPotion_MP , Itm::TrumpTonic],
	  7 => [Itm::HiPotion , Itm::HiPotion_MP , Itm::TrumpTonic],
	  8 => [Itm::HiPotion , Itm::MegaPotion , Itm::TrumpTonic],
	  9 => [Itm::HiPotion , Itm::MegaPotion , Itm::TrumpTonic],
	  10 => [Itm::HiPotion , Itm::MegaPotion_MP , Itm::TrumpTonic],
	  11 => [Itm::MegaPotion , Itm::MegaPotion_MP , Itm::TrumpTonic],
	  12 => [Itm::MegaPotion , Itm::MegaPotion_MP , Itm::TrumpTonic],
	  13 => [Itm::MegaPotion , Itm::TrumpTonic , Itm::TrumpTonic],
	  14 => [Itm::MegaPotion , Itm::TrumpTonic , Itm::TrumpTonic],
	  15 => [Itm::MegaPotion_MP , Itm::TrumpTonic , Itm::TrumpTonic],
	  16 => [Itm::MegaPotion_MP , Itm::TrumpTonic , Itm::TrumpTonic],
	  17 => [Itm::MegaPotion_MP , Itm::TrumpTonic , Itm::TrumpTonic],
	  18 => [Itm::MegaPotion , Itm::MegaPotion_MP , Itm::MegaPotion_MP],
	  19 => [Itm::MegaPotion , Itm::MegaPotion_MP , Itm::MegaPotion_MP],
	  20 => [Itm::MegaPotion , Itm::MegaPotion_MP , Itm::MegaPotion_MP],
	  21 => [Itm::MegaPotion , Itm::MegaPotion_MP , Itm::MegaPotion_MP],
	  22 => [Itm::MegaPotion , Itm::MegaPotion_MP , Itm::MegaPotion_MP],
	  23 => [Itm::MegaPotion , Itm::MegaPotion_MP , Itm::MegaPotion_MP],
	  24 => [Itm::MegaPotion , Itm::MegaPotion_MP , Itm::MegaPotion_MP],
	  25 => [Itm::MegaPotion , Itm::MegaPotion_MP , Itm::MegaPotion_MP],
	}
	
	#floor->{C,B,A,S}
	@@weapon_baselines = {
	  1 => [Wpn::PizFrk1 , Wpn::PizFrk2 , Wpn::PizFrk3 , Wpn::PizFrk_MAX],
	  2 => [Wpn::PizFrk1 , Wpn::PizFrk2 , Wpn::Prize1 , Wpn::PizFrk_MAX],
	  3 => [Wpn::PizFrk1 , Wpn::PizFrk3 , Wpn::Prize1 , Wpn::PizFrk_MAX],
	  4 => [Wpn::PizFrk2 , Wpn::PizFrk3 , Wpn::Prize1 , Wpn::PizFrk_MAX],
	  5 => [Wpn::PizFrk2 , Wpn::Prize1 , Wpn::Prize2 , Wpn::PizFrk_MAX],
	  6 => [Wpn::PizFrk3 , Wpn::Prize1 , Wpn::Prize2 , Wpn::PizFrk_MAX],
	  7 => [Wpn::PizFrk3 , Wpn::Prize2 , Wpn::Prize3 , Wpn::PizFrk_MAX],
	  8 => [Wpn::PizFrk3 , Wpn::Prize2 , Wpn::Prize3 , Wpn::Prize_MAX],
	  9 => [Wpn::Prize1 , Wpn::Prize2 , Wpn::Prize3 , Wpn::Prize_MAX],
	  10 => [Wpn::Prize1 , Wpn::Prize2 , Wpn::Star1 , Wpn::Prize_MAX],
	  11 => [Wpn::Prize1 , Wpn::Prize3 , Wpn::Star1 , Wpn::Prize_MAX],
	  12 => [Wpn::Prize2 , Wpn::Prize3 , Wpn::Star1 , Wpn::Prize_MAX],
	  13 => [Wpn::Prize2 , Wpn::Prize3 , Wpn::Star2 , Wpn::Prize_MAX],
	  14 => [Wpn::Prize3 , Wpn::Prize3 , Wpn::Star2 , Wpn::Prize_MAX],
	  15 => [Wpn::Prize3 , Wpn::Star1 , Wpn::Star2 , Wpn::Prize_MAX],
	  16 => [Wpn::Prize3 , Wpn::Star1 , Wpn::Star2 , Wpn::Star_MAX],
	  17 => [Wpn::Star1 , Wpn::Star2 , Wpn::Star2 , Wpn::Star_MAX],
	  18 => [Wpn::Star1 , Wpn::Star2 , Wpn::Star3 , Wpn::Star_MAX],
	  19 => [Wpn::Star2 , Wpn::Star3 , Wpn::Star3 , Wpn::Star_MAX],
	  20 => [Wpn::Star3 , Wpn::Star3 , Wpn::Star3 , Wpn::Star_MAX],
	  
	  #TODO: Custom weapons, later.
	  21 => [Wpn::Star1 , Wpn::Star2 , Wpn::Star3 , Wpn::Star_MAX],
	  22 => [Wpn::Star2 , Wpn::Star3 , Wpn::Star3 , Wpn::Star_MAX],
	  23 => [Wpn::Star3 , Wpn::Star3 , Wpn::Star3 , Wpn::Star_MAX],
	  24 => [Wpn::Star1 , Wpn::Star2 , Wpn::Star3 , Wpn::Star_MAX],
	  25 => [Wpn::Star2 , Wpn::Star3 , Wpn::Star3 , Wpn::Star_MAX],
	}
	
    #TEMP: Re-using probabilities for now...
	#floor->{C,B,A,S}
	@@hair_baselines = {
	  1 => [Hair::PizFrk1 , Hair::PizFrk2 , Hair::PizFrk3 , Hair::PizFrk_MAX],
	  2 => [Hair::PizFrk1 , Hair::PizFrk2 , Hair::Prize1 , Hair::PizFrk_MAX],
	  3 => [Hair::PizFrk1 , Hair::PizFrk3 , Hair::Prize1 , Hair::PizFrk_MAX],
	  4 => [Hair::PizFrk2 , Hair::PizFrk3 , Hair::Prize1 , Hair::PizFrk_MAX],
	  5 => [Hair::PizFrk2 , Hair::Prize1 , Hair::Prize2 , Hair::PizFrk_MAX],
	  6 => [Hair::PizFrk3 , Hair::Prize1 , Hair::Prize2 , Hair::PizFrk_MAX],
	  7 => [Hair::PizFrk3 , Hair::Prize2 , Hair::Prize3 , Hair::PizFrk_MAX],
	  8 => [Hair::PizFrk3 , Hair::Prize2 , Hair::Prize3 , Hair::Prize_MAX],
	  9 => [Hair::Prize1 , Hair::Prize2 , Hair::Prize3 , Hair::Prize_MAX],
	  10 => [Hair::Prize1 , Hair::Prize2 , Hair::Star1 , Hair::Prize_MAX],
	  11 => [Hair::Prize1 , Hair::Prize3 , Hair::Star1 , Hair::Prize_MAX],
	  12 => [Hair::Prize2 , Hair::Prize3 , Hair::Star1 , Hair::Prize_MAX],
	  13 => [Hair::Prize2 , Hair::Prize3 , Hair::Star2 , Hair::Prize_MAX],
	  14 => [Hair::Prize3 , Hair::Prize3 , Hair::Star2 , Hair::Prize_MAX],
	  15 => [Hair::Prize3 , Hair::Star1 , Hair::Star2 , Hair::Prize_MAX],
	  16 => [Hair::Prize3 , Hair::Star1 , Hair::Star2 , Hair::Star_MAX],
	  17 => [Hair::Star1 , Hair::Star2 , Hair::Star2 , Hair::Star_MAX],
	  18 => [Hair::Star1 , Hair::Star2 , Hair::Star3 , Hair::Star_MAX],
	  19 => [Hair::Star2 , Hair::Star3 , Hair::Star3 , Hair::Star_MAX],
	  20 => [Hair::Star3 , Hair::Star3 , Hair::Star3 , Hair::Star_MAX],
	  
	  #TODO: Custom weapons, later.
	  21 => [Hair::Star1 , Hair::Star2 , Hair::Star3 , Hair::Star_MAX],
	  22 => [Hair::Star2 , Hair::Star3 , Hair::Star3 , Hair::Star_MAX],
	  23 => [Hair::Star3 , Hair::Star3 , Hair::Star3 , Hair::Star_MAX],
	  24 => [Hair::Star1 , Hair::Star2 , Hair::Star3 , Hair::Star_MAX],
	  25 => [Hair::Star2 , Hair::Star3 , Hair::Star3 , Hair::Star_MAX],
	}
	#floor->{C,B,A,S}
	@@armor_baselines = {
	  1 => [Armr::PizFrk1 , Armr::PizFrk2 , Armr::PizFrk3 , Armr::PizFrk_MAX],
	  2 => [Armr::PizFrk1 , Armr::PizFrk2 , Armr::Prize1 , Armr::PizFrk_MAX],
	  3 => [Armr::PizFrk1 , Armr::PizFrk3 , Armr::Prize1 , Armr::PizFrk_MAX],
	  4 => [Armr::PizFrk2 , Armr::PizFrk3 , Armr::Prize1 , Armr::PizFrk_MAX],
	  5 => [Armr::PizFrk2 , Armr::Prize1 , Armr::Prize2 , Armr::PizFrk_MAX],
	  6 => [Armr::PizFrk3 , Armr::Prize1 , Armr::Prize2 , Armr::PizFrk_MAX],
	  7 => [Armr::PizFrk3 , Armr::Prize2 , Armr::Prize3 , Armr::PizFrk_MAX],
	  8 => [Armr::PizFrk3 , Armr::Prize2 , Armr::Prize3 , Armr::Prize_MAX],
	  9 => [Armr::Prize1 , Armr::Prize2 , Armr::Prize3 , Armr::Prize_MAX],
	  10 => [Armr::Prize1 , Armr::Prize2 , Armr::Star1 , Armr::Prize_MAX],
	  11 => [Armr::Prize1 , Armr::Prize3 , Armr::Star1 , Armr::Prize_MAX],
	  12 => [Armr::Prize2 , Armr::Prize3 , Armr::Star1 , Armr::Prize_MAX],
	  13 => [Armr::Prize2 , Armr::Prize3 , Armr::Star2 , Armr::Prize_MAX],
	  14 => [Armr::Prize3 , Armr::Prize3 , Armr::Star2 , Armr::Prize_MAX],
	  15 => [Armr::Prize3 , Armr::Star1 , Armr::Star2 , Armr::Prize_MAX],
	  16 => [Armr::Prize3 , Armr::Star1 , Armr::Star2 , Armr::Star_MAX],
	  17 => [Armr::Star1 , Armr::Star2 , Armr::Star2 , Armr::Star_MAX],
	  18 => [Armr::Star1 , Armr::Star2 , Armr::Star3 , Armr::Star_MAX],
	  19 => [Armr::Star2 , Armr::Star3 , Armr::Star3 , Armr::Star_MAX],
	  20 => [Armr::Star3 , Armr::Star3 , Armr::Star3 , Armr::Star_MAX],
	  
	  #TODO: Custom weapons, later.
	  21 => [Armr::Star1 , Armr::Star2 , Armr::Star3 , Armr::Star_MAX],
	  22 => [Armr::Star2 , Armr::Star3 , Armr::Star3 , Armr::Star_MAX],
	  23 => [Armr::Star3 , Armr::Star3 , Armr::Star3 , Armr::Star_MAX],
	  24 => [Armr::Star1 , Armr::Star2 , Armr::Star3 , Armr::Star_MAX],
	  25 => [Armr::Star2 , Armr::Star3 , Armr::Star3 , Armr::Star_MAX],
	}
	
    #Does the heavy lifting.
    #Returns:
    #  type = 0,1,2 for weapon/armor/item
    #  id = the weapon/armor/item ID
    #  count = the number of items
    #Input:
    #  level = the current floor we're on.
    #  type = S,A,B,C for quality
    #  cat = category A,D,R,I,S,M (attack, defense, relic, item, skill misc)
    def calc_treasure(level, type, cat)
	  #Fail-safe
	  if !(@@items_per_floor.has_key?(level) && @@weapon_baselines.has_key?(level) && @@hair_baselines.has_key?(level) && @@armor_baselines.has_key?(level))
	    puts "ERRO: MISSING BASELINE FOR WEAPONS."
	    return 2 , Itm::Potion , 1
	  end
	  
	  lookup = {
	    "C" => 0,
		"B" => 1,
		"A" => 2,
		"S" => 3,
	  }
	  
	  #TEMP: Reset unknown categories to "hair" (relic) since it's rare.
	  if cat=="S"
	    cat = "I"
		cat = "R" if level > 10 #No more hand-holding for items.
	  end
	  cat = "R" if cat=="M"
	
      #Items are pretty simple; do those first.
	  #Items never take rarity into account.
      if cat == "I"
	    #Get a lookup index.
        v = @@my_rand.rand
		if v<0.08
		  v = 2
		elsif v<0.2
		  v = 1
		else
		  v = 0
		end
		
		id = @@items_per_floor[level][v]
		count = 1
		if v==1
		  count = uniform(1,3)
		elsif v==2
		  count = uniform(3,5)
		end
		
		return 2, id, count
      end
	  
	  
	  #Weapons aren't too hard; there's just a little more automation here.
	  if cat == "A"
	    #Here, the level determines what the rarity means.
		id = @@weapon_baselines[level][lookup[type]]
		
		#Modify based on rarity.
		if @@my_rand.rand < 0.75
		  if type=="A"
  		    #Wind/elect
			if @@my_rand.rand < 0.5
			  id += 10
      	    else
			  id += 13
			end
		  elsif type=="B"
  		    #Fire/ice
			if @@my_rand.rand < 0.5
			  id += 4
      	    else
			  id += 7
			end
		  end
		end
	    return 0, id, 1 #Always count=1
	  end
	  
	  #Hair ("relics") follow the same formula
	  if cat == "R"
	    #Here, the level determines what the rarity means.
		id = @@hair_baselines[level][lookup[type]]
		
		#Modify based on rarity.
		if @@my_rand.rand < 0.75
		  if type=="A"
  		    #Wind/elect
			if @@my_rand.rand < 0.5
			  id += 10
      	    else
			  id += 13
			end
		  elsif type=="B"
  		    #Fire/ice
			if @@my_rand.rand < 0.5
			  id += 4
      	    else
			  id += 7
			end
		  end
		end
	    return 1, id, 1 #Always count=1
	  end

	  #Armor follows the same formula
	  if cat == "D"
	    #Here, the level determines what the rarity means.
		id = @@armor_baselines[level][lookup[type]]
		
		#Modify based on rarity.
		if @@my_rand.rand < 0.75
		  if type=="A"
  		    #Wind/elect
			if @@my_rand.rand < 0.5
			  id += 10
      	    else
			  id += 13
			end
		  elsif type=="B"
  		    #Fire/ice
			if @@my_rand.rand < 0.5
			  id += 4
      	    else
			  id += 7
			end
		  end
		end
	    return 1, id, 1 #Always count=1
	  end
	  
      puts "ERRO: GOT TO END OF TREASURE FUNCTION."
	  return 2 , Itm::Potion , 1
	  
    end
	
	

    #Sets several tiles to RoomDefect1 (which follow the door pathways) or RoomDefect2 (which do not)
    def defect() 
      rm_grps = [Tile::RmGrp1,Tile::RmGrp2,Tile::RmGrp3,Tile::RmGrp4,Tile::RmGrp5,Tile::RmGrp6]
      for y in 0..@map.height
        for x in 0..@map.width
		  #Replace all RmGrp's AROUND non-room-groups with actual RmWall labels.
          nonGrp = !(@map.is_tile?(x, y, rm_grps) || @map.is_tile?(x, y, Tile::RmWall))
		  nWall = @map.is_item?(x,y,Item::NorthWall)
          if nonGrp || nWall
            #Check and set around it.
            for i in 1..9
              off = [x,y]
              off[0] += 1 if [3,6,9].include? i
              off[0] -= 1 if [1,4,7].include? i
              off[1] += 1 if [7,8,9].include? i
              off[1] -= 1 if [1,2,3].include? i
              if @map.is_tile?(off[0], off[1], rm_grps) || @map.is_item?(off[0],off[1],Item::NorthWall)
                @map.data[off[0]][off[1]][0] = Tile::RmWall
              end
            end
          end
		
          #Defect ROOMS
          if @map.is_tile? x, y, [Tile::Room]
            #First, try making a non-pathing tile. These tend to cluster a little.
            chance = 0.08
            for i in 1..4
              off = [x,y]
              off[0] += 1 if i==1
              off[0] -= 1 if i==2
              off[1] += 1 if i==3
              off[1] -= 1 if i==4
              chance += 0.08 if @map.is_tile? off[0], off[1], Tile::RoomDefect2
            end
            @map.data[x][y][0] = Tile::RoomDefect2 if @@my_rand.rand <= chance

			#Next, possibly overlap defects if they are close to the path.
            chance = 0.0
			for i in 1..9
              off = [x,y]
              off[0] += 1 if [3,6,9].include? i
              off[0] -= 1 if [1,4,7].include? i
              off[1] += 1 if [7,8,9].include? i
              off[1] -= 1 if [1,2,3].include? i
              if @map.is_item? off[0], off[1], Item::DefaultPath
			    chance += 0.1
                chance += 0.3 if off[0]==x && off[1]==y
              end
			end
            @map.data[x][y][0] = Tile::RoomDefect1 if @@my_rand.rand <= chance
          end
		  
          #Defect PITS (water)
          if @map.is_tile?(x, y, [Tile::Pit])
            #See if we've got any nearby walls/bridges (we don't want that)
            failEarly = false
			for i in 1..9
              off = [x,y]
              off[0] += 1 if [3,6,9].include? i
              off[0] -= 1 if [1,4,7].include? i
              off[1] += 1 if [7,8,9].include? i
              off[1] -= 1 if [1,2,3].include? i
              failEarly = true unless @map.is_tile? off[0], off[1], [Tile::Pit,Tile::WaterDefect1,Tile::WaterDefect2]
              failEarly = true if @map.is_item? off[0], off[1], Item::NorthWall
            end
			
			if !failEarly
              #Next try spotting edge tiles.These are closer to (obviously) the edge.
			  if @@my_rand.rand<0.5  #Some rooms have no WaterDefect2 tiles
                chance = 0.00
			    for i in 1..9
                  off = [x,y]
                  off[0] += 2 if [3,6,9].include? i
                  off[0] -= 2 if [1,4,7].include? i
                  off[1] += 2 if [7,8,9].include? i
                  off[1] -= 2 if [1,2,3].include? i
                  chance += 0.03 if !@map.is_tile? off[0], off[1], [Tile::Pit,Tile::WaterDefect1,Tile::WaterDefect2]
                  chance += 0.01 if @map.is_tile? off[0], off[1], Tile::WaterDefect2
                end
                @map.data[x][y][0] = Tile::WaterDefect2 if @@my_rand.rand <= chance
              end

              #Next, try spotting random tiles. These are slightly more likely in the center.
              chance = 0.04
			  for i in 1..9
                off = [x,y]
                off[0] += 1 if [3,6,9].include? i
                off[0] -= 1 if [1,4,7].include? i
                off[1] += 1 if [7,8,9].include? i
                off[1] -= 1 if [1,2,3].include? i
                chance += 0.01 if @map.is_tile? off[0], off[1], [Tile::Pit,Tile::WaterDefect1,Tile::WaterDefect2]
			    chance += 0.03 if @map.is_tile? off[0], off[1], Tile::WaterDefect1
              end
              @map.data[x][y][0] = Tile::WaterDefect1 if @@my_rand.rand <= chance
            end
          end
        end
      end
	  
      #Finally, make ceilings.
      for y in 0..@map.height
        for x in 0..@map.width
          if @map.is_tile?(x, y, rm_grps)
            #Anything left over is a ceiling.
            @map.data[x][y][0] = Tile::RmCeiling
          end
		  
		  #Grumble
          if @map.is_item?(x, y, Item::NorthWall)
            @map.data[x][y][0] = Tile::RmGrp1
          end
        end
      end
    end
	
	

    #Cleanup and tag a few more northwall tiles (and others!)
    def cleanup_tiles()
      for y in 1..@map.height
        for x in 0..@map.width
          #We shoe-horn a check for bridges here...
          if @map.is_tile? x, y, [Tile::Bridge,Tile::Tunnel]
            @map.data[x][y][0] = Tile::Tunnel
            @map.data[x][y][0] = Tile::Bridge if @map.is_tile? x+1, y, Tile::Pit
            @map.data[x][y][0] = Tile::Bridge if @map.is_tile? x-1, y, Tile::Pit
            @map.data[x][y][0] = Tile::Bridge if @map.is_tile? x, y+1, Tile::Pit
            @map.data[x][y][0] = Tile::Bridge if @map.is_tile? x, y-1, Tile::Pit
          end

          #Water northwall
          if @map.is_tile?(x, y, Tile::Pit) && @map.is_tile?(x, y-1, [Tile::RmGrp1,Tile::RmGrp2,Tile::RmGrp3,Tile::RmGrp4,Tile::RmGrp5,Tile::RmGrp6])
            @map.data[x][y][1] = Item::NorthWall
          end

          #Tunnel/Bridge northwall.
          next unless @map.is_tile?(x, y, [Tile::Bridge,Tile::Tunnel])
          next unless @map.is_tile?(x, y-1, [Tile::RmGrp1,Tile::RmGrp2,Tile::RmGrp3,Tile::RmGrp4,Tile::RmGrp5,Tile::RmGrp6])

          #If there's a room above this, try to give that room a block.
          if @map.is_tile?(x, y-2, [Tile::Room,Tile::Pit])
            if @map.is_item?(x, y-2, Item::Empty)
              @map.data[x][y-2][0] = @map.data[x][y-1][0]
            end
          end

          #Ok, it's a Northwall
          @map.data[x][y-1][1] = Item::NorthWall
        end
      end
    end


    #Walk through our rooms one-by-one and assign things like monsters, treasures, locked doors, starting/ending positions, etc.
    #We ensure that each item has a location that does not impede the walking path.
    #(The actual GameEngine may or may not use these.)
    def walk_and_proc_rooms()
      #Get the starting room (where we teleport in).
      startRoom = sample(@room_paths.keys)

      #Put the starting (teleport in) area somewhere on the default path, preferring the center of the room.
      tel_in_tile = pick_center_default(startRoom)

      #Set it.
      @map.data[tel_in_tile[0]][tel_in_tile[1]][1] = Item::StartLocation

      #Now that we've got the starting room, let's build out. We definitely want to BFS this.
      completed_rooms = {} #rm->1
      to_process = [startRoom] #rm,rm... to process
      set_down_staircase = false
      while !to_process.empty?
        #Get the next room, mark it.
        rm = to_process.shift
        completed_rooms[rm] = 1

        #First, add its neighbors to the list of completed rooms.
        addedNone = true
        for otherRm in @room_paths[rm]
          next if otherRm == rm
          next if completed_rooms.include? otherRm
          next if to_process.include? otherRm
          to_process.push otherRm
          addedNone = false
        end

        #With some chance (20%), if we are in a dead-end, set the down staircase
        if (addedNone && @@my_rand.rand<0.2 && !set_down_staircase)
          add_down_staircase(rm)
          set_down_staircase = true
        end

        #Vary it. 
        vary_room(rm)

        #With some chance (based on how many rooms have already been processed), lock
        # one of this room's doors and put the key in a completed room.
        #TODO: Might not matter for now...

        #Build its north wall (treasure shouldn't be there anyway)
        build_n_wall(rm)
      end

      #Finally, set the down staircase if none has been set.
      if !set_down_staircase
        add_down_staircase(sample(@room_paths.keys))
      end
    end

    def build_n_wall(rm)
      #We do this generically, since the room may have eroded.
      for x in rm.lx..rm.hx
        for y in rm.ly..rm.hy
          #Check the walls now
          must_check = @map.valid_tile? x, y-1
          next unless @map.is_tile? x, y, Tile::Room
          if must_check
            next unless @map.is_tile? x, y-1, [Tile::RmGrp1,Tile::RmGrp2,Tile::RmGrp3,Tile::RmGrp4,Tile::RmGrp5,Tile::RmGrp6]
            if !(@map.is_item?(x,y-1, Item::Empty) && @map.is_item?(x,y, Item::Empty))
              puts "UNEXPECTED north wall conflict!"
              next
            end
          end
          @map.data[x][y][1] = Item::NorthWall
        end
      end
    end


    #Retrieve a default-path tile that is not over the door, preferring those in the center of the room.
    def pick_center_default(rm)
      res = nil #[x,y], start location.
      backup_tiles = [] #Just in case
      for x in 0..rm.width/2
        for y in 0..rm.height/2
          #We consider 9 tiles (+/- x/y, and 0)
          for i in 1..9
            tile = [rm.lx+rm.width/2,rm.ly+rm.height/2]
            tile[0] += x if [3,6,9].include? i
            tile[0] -= x if [1,4,7].include? i
            tile[1] += y if [7,8,9].include? i
            tile[1] -= y if [1,2,3].include? i
            next if @map.is_tile? tile[0], tile[1], Tile::Door
            next unless @map.is_item? tile[0], tile[1], Item::DefaultPath
            next unless tile[0]>=rm.lx && tile[0]<=rm.hx && tile[1]>=rm.ly && tile[1]<=rm.hy
            if @@my_rand.rand < 0.25
              res = tile
              break
            else
              backup_tiles.push tile
            end
          end
          break unless res.nil?
        end
        break unless res.nil?
      end

      #Last ditch effort
      if res.nil?
        if backup_tiles.empty?
          #Uh-oh...
          puts "MASSIVE ERROR: No possible starting location!"
          res = [uniform(rm.lx+1,rm.hx-1),uniform(rm.ly+1,rm.hy-1)]
        else
          res = sample(backup_tiles)
        end
      end

      return res
    end


    #Place the "down" staircase in this room
    def add_down_staircase(rm)
      #We really want to put the staircase anywhere (but *probably) not directly on the default path)
      for i in 1..50
        tile = [uniform(rm.lx+1, rm.hx-1), uniform(rm.ly+1, rm.hy-1)]
        break unless @map.is_item?(tile[0], tile[1], Item::DefaultPath)
      end

      #Set it, then build out to a random tile in the default path.
      @map.data[tile[0]][tile[1]][1] = Item::StaircaseDown
      goal = rm.goal

      #Now build out until we hit the default path.
      walk_to_default(rm, tile, goal)
    end


    #Walk from "tile" to "goal" (within rm), spotting the default path and stopping if we hit the default path.
    def walk_to_default(rm, tile, goal)
      #We need a set of directions.
      directions = [] #[S,10] = walk S 10 steps, etc.

      #Regardless of our position, we have to build "straight" out to the tile first.
      if tile[0]==rm.lx || tile[0]==rm.hx || tile[1]==goal[1]
        buildHoriz = true
      elsif tile[1]==rm.ly || tile[1]==rm.hy || tile[0]==goal[0]
        buildHoriz = false
      else
        buildHoriz = @@my_rand.rand < 0.5
      end

      if buildHoriz
        #First, build horizontally
        if tile[0]>goal[0]
          directions.push ["W", tile[0]-goal[0]]
        elsif goal[0]>tile[0]
          directions.push ["E", goal[0]-tile[0]]              
        else
          puts "WARNING: Can't draw default path (horiz.)"
          return
        end

        #Next, build vertically (unless we are already in line)
        if tile[1] != goal[1]
          if tile[1]>goal[1]
            directions.push ["N", tile[1]-goal[1]]
          else
            directions.push ["S", goal[1]-tile[1]]              
          end
        end
      else
        #First, build vertically.
        if tile[1]>goal[1]
          directions.push ["N", tile[1]-goal[1]]
        elsif goal[1]>tile[1]
          directions.push ["S", goal[1]-tile[1]]              
        else
          puts "WARNING: Can't draw default path (vert.)"
          return
        end

        #Next, build horizontally (unless we are already in line)
        if tile[0] != goal[0]
          if tile[0]>goal[0]
            directions.push ["W", tile[0]-goal[0]]
          else
            directions.push ["E", goal[0]-tile[0]]              
          end
        end
      end

      #Now, build!
      currTile = [tile[0],tile[1]]
      for dir in directions
        #Stop early?
        break if @map.is_item? currTile[0],currTile[1],Item::DefaultPath
        for i in 1..dir[1]
          #Spot
          @map.data[currTile[0]][currTile[1]][1] = Item::DefaultPath if @map.is_item?(currTile[0], currTile[1], Item::Empty)

          #Move
          currTile[0] += 1 if dir[0]=="E"
          currTile[0] -= 1 if dir[0]=="W"
          currTile[1] += 1 if dir[0]=="S"
          currTile[1] -= 1 if dir[0]=="N"

          #Stop early?
          break if @map.is_item?(currTile[0],currTile[1],Item::DefaultPath) || @map.is_item?(currTile[0],currTile[1],Item::SpecialSpot1)
        end
      end
    end


    def vary_room(rm)
      #We need to place monsters, treasure, and npcs. However, not all rooms need all of each.
      #We also want to take room groups into account, as well as room sizes, etc.
      my_grp = nil
      my_grp_id = 1  #TEMP way of enforcing theme.
      @room_groups.each {|grp|
        grp.rooms.each {|otherRm|
          if otherRm == rm
            my_grp = grp
            break
          end
        }
        my_grp_id += 1 if my_grp.nil?
      }
      my_grp_id = [my_grp_id,5].min

      #Our action items are (in order) setting monsters (M), setting treasures (T), setting npcs (N), and doing nothing (X).
      #They each have tokens associated with them (0+), where >0 tokens are needed for that event to occur.
      #Each action (see below) removes a token if that action item is selected. When a given action item reaches 1,
      #  all items have their values multiplied by 10. 
      action_items = get_action_items(my_grp_id)

      #When we place items, we have a given "pattern". This is reset when the mode changes, and allows us to group items across same-mode batches.
      my_placement = ["T",[]] #mode, [allowed places]

      #Each quad of an Ideal Room allows 1 action. We then divide the 
      #room's total area by the action area and round up to get the 
      #count of allowed actions (an action = a treasure/monster/npc placement).
      action_area = (15/2) * (12/2) * 1.0
      action_count = ((rm.width*rm.height)/action_area).ceil
      #puts "Total actions: #{action_count}"
	  
	  #Place the teleporter FIRST
	  blacklist2 = {} #Grrr...
	  for fromRm,toRm in @map.teleports
	    next unless fromRm==rm
	    next if blacklist2.include? [toRm,fromRm]
		blacklist2[[fromRm,toRm]] = true
		blacklist2[[toRm,fromRm]] = true
		
		#Pick both points.
		rm1tel = nil
		rm2tel = nil
		for i in 1..50
		  if rm1tel.nil?
		    new_x = uniform(fromRm.lx+1, fromRm.hx-1)
		    new_y = uniform(fromRm.ly+2, fromRm.hy-1)
		   if  @map.is_item?(new_x, new_y, Item::Empty) || @map.is_item?(new_x, new_y, Item::DefaultPath) 
		      rm1tel = [new_x, new_y]
		    end
		  end
		  if rm2tel.nil?
		    new_x = uniform(toRm.lx+1, toRm.hx-1)
		    new_y = uniform(toRm.ly+2, toRm.hy-1)
		   if  @map.is_item?(new_x, new_y, Item::Empty) || @map.is_item?(new_x, new_y, Item::DefaultPath) 
		      rm2tel = [new_x, new_y]
		    end
		  end
		  break unless rm1tel.nil? || rm2tel.nil?
		end
		
		if rm1tel.nil? || rm2tel.nil?
		  puts "ERRRRRRRRRRROR; couldn't set teleporters"
		else
		  @map.teleport_pos[rm1tel] = rm2tel
		  @map.teleport_pos[rm2tel] = rm1tel
		  @map.data[rm1tel[0]][rm1tel[1]][1] = Item::Teleporter
		  @map.data[rm2tel[0]][rm2tel[1]][1] = Item::Teleporter
		end
	  end
	  

      #With a VERY rare chance, swap M and T for this entire room
      swap_mt = false
      if uniform(1,100) < 5
        swap_mt = true
      end

      for i in 1..action_count
        #Get an action ("M","T","N",X")
        action = select_action(action_items, ["M", "T", "N", "X"])
        if swap_mt
          if action=="M"
            action="T"
          elsif action=="T"
            action="M"
          end
        end

        #puts "  Chose action: #{action}"

        #Reset placement?
        next unless check_and_set_placement(my_placement, action, rm)

        #React appropriately
        if action=="M"
          #Monsters.
          count = get_monster_count(my_grp_id)
          for i in 1..count
            #Monster rank (S,A,B,C) and position
            next unless check_and_set_placement(my_placement, action, rm)
            value = get_monster_value(my_grp_id)
            place = my_placement[1].shift

            #Set it.
            @map.data[place[0]][place[1]][1] = Item::Monster
            @map.monsters[place] = value
          end
        elsif action=="T"
          #Treasures
          count = get_treasure_count(my_grp_id)
          for i in 1..count
            #Treasure ranks (S,A,B,C),(A,D,R,I,S,M) and position
            next unless check_and_set_placement(my_placement, action, rm)
            values = get_treasure_values(my_grp_id)
            place = my_placement[1].shift

            #Set it.
            @map.data[place[0]][place[1]][1] = Item::Treasure
            @map.treasures[place] = values
          end
        elsif action=="N"
          #NPCs
          #TODO
        elsif action=="X"
          #Skip action this turn.
        else
          puts "ERROR: Unknown action: #{action}"
        end
      end




      #TODO: This is where we assign room properties.

    end


    #Picks an action given the set of (M,T,N,X) action items. 
    #Decrements the winner (and auto-multiplies, if needed). 
    #Skips zero-token items.
    def select_action(action_items, action_indices)
      #First, we normalize.
      total_tokens = 0.0
      for prob in action_items
        total_tokens += prob
      end

      #Now, choose. 
      r = @@my_rand.rand(1.0)
      tot = 0.0
      for chosen_index in 0...action_items.length
        tot += action_items[chosen_index]/total_tokens
        break if r < tot
      end

      #Decrement, mult
      action_items[chosen_index] -= 1
      if action_items[chosen_index]==1
        for i in 0...action_items.length
          action_items[i] *= 10
        end
      end
      return action_indices[chosen_index]
    end




    #Major Phase 1: After this phase, we should have a set of RoomGroups and Rooms, with paths connecting them, and 
    #               teleporters for any unconnected rooms. No actual objects have been placed yet.
    def generate_rooms()
      #We prefer rectangular rooms
      ratio = ((@ideal_dungeon.max / @ideal_dungeon.min.to_f) - 1.0) * 1.1
      randrat = triangle(0, ratio, 0.75*ratio)
      base = @ideal_dungeon.min.to_f
      base_dungeon = [base * (1.0+randrat), base * (1.0+ratio-randrat)]
      base_dungeon = [base_dungeon[1], base_dungeon[0]] if @@my_rand.rand < 0.5 #rectangle is horiz or vert.?
      base_dungeon = [[@max_dungeon[0],base_dungeon[0].to_i].min, [@max_dungeon[1],base_dungeon[1].to_i].min]
      puts "Creating dungeon of size: #{base_dungeon[0]} x #{base_dungeon[1]}"
      @map = DummyMap.new base_dungeon[0], base_dungeon[1]

      #Now, split the room randomly between 0 and 4 times, creating our RoomGroups
      splits = [0.01, 0.18, 0.7, 0.99, 1.0]  #0,4 are super-rare
      uni = @@my_rand.rand
      if uni<splits[0]
        splits = 0
      elsif uni<splits[1]
        splits = 1
      elsif uni<splits[2]
        splits = 2
      elsif uni<splits[3]
        splits = 3
      else
        splits = 4
      end

      #Perform the splits, picking a random RoomGroup to split each time.
      @room_groups = []
      split_prefer = 0.5  #less->horiz
      add_room_group(0, 0, @map.width, @map.height)
      for i in 1..splits
        #Pick a random rect from the list to split
        rect = sample(@room_groups)
        #puts "Splitting: #{rect.lx},#{rect.ly} => #{rect.hx},#{rect.hy}"

        #Split
        if @@my_rand.rand < split_prefer
          #Horiz
          split_prefer -= 0.15
          bar = rect.hx-rect.lx
          bar = uniform(0.4*bar, 0.6*bar) + rect.lx
          #puts "Split horiz: #{bar}"
          add_room_group(bar+1, rect.ly, rect.hx, rect.hy)
          rect.hx = bar
          #puts "Updated Rect: #{rect.lx},#{rect.ly} => #{rect.hx},#{rect.hy}"
        else
          #Vert
          split_prefer += 0.15
          bar = rect.hy-rect.ly
          bar = uniform(0.4*bar, 0.6*bar) + rect.ly
          #puts "Split vert: #{bar}"
          add_room_group(rect.lx, bar+1, rect.hx, rect.hy)
          rect.hy = bar
          #puts "Updated Rect: #{rect.lx},#{rect.ly} => #{rect.hx},#{rect.hy}"
        end
      end

      #We paint RoomGroups for easy debugging.
      tileIds = [Tile::RmGrp1, Tile::RmGrp2, Tile::RmGrp3, Tile::RmGrp4, Tile::RmGrp5, Tile::RmGrp6, Tile::Unknown]
      tileId = 0
      for rect in @room_groups
        for x in rect.lx..rect.hx
          for y in rect.ly..rect.hy
            raise "oops" if @map.data[x][y][0] != Tile::Empty
            @map.data[x][y][0] = tileIds[tileId]
          end
        end
        tileId += 1
      end

      #Now, for reach RoomGroup, we need to generate rooms via clustering.
      puts "Generating rooms..."
      @room_groups.each {|rect|
        #We create anywhere from 0 to X rooms, where X is determined by comparing the area of the rect to the area of the room
        maxRm = ((rect.hx-rect.lx)*(rect.hy-rect.ly)) / (@ideal_room[0]*@ideal_room[1])
        maxRm = [1, (maxRm*0.65).to_i].max
        roomOpts = []
        count = 1
        halfRm = (maxRm/2.0).ceil
        for i in 1..halfRm
          for j in 1..count
            roomOpts.push i
            roomOpts.push maxRm-i+1  if maxRm-i+1 != i
          end
          count *= 2
        end

        #Now, generate that many rooms (slightly bigger)
        numRooms = sample(roomOpts)
        #puts "New rect; #{numRooms} rooms"
        room_sizes = [(@ideal_room[0]*1.1).to_i, (@ideal_room[1]*1.1).to_i]
        for i in 1..numRooms
          #First, choose a width/height
          #For now, we uniformly accept from 0.6 to 1.35 times the ideal room size,
          #  although we'll want some consideration for "huge" and "tiny" rooms later.
          const1 = 0.6
          const2 = 1.35
          rm_sz = [uniform(const1*room_sizes[0], const2*room_sizes[0]), uniform(const1*room_sizes[1], const2*room_sizes[1])]
          rm_sz = [rm_sz[1], rm_sz[0]] if @@my_rand.rand<0.5 #Flip h/v

          #We have to account for the differences between Kernel.rand and Random.rand
          #TODO: We might actually want to fix this to center the rooms better, but right now I like how they shift together.
          to_gen = [rect.hx-rect.lx-rm_sz[0] , rect.hy-rect.ly-rm_sz[1]]
          to_gen[0] = to_gen[0].abs
          to_gen[1] = to_gen[1].abs

          #We also place it uniformly in the rect (minus the bounds)
          center = [uniform(0, to_gen[0]), uniform(0, to_gen[1])]
          center = [rect.lx+center[0]+rm_sz[0]/2, rect.ly+center[1]+rm_sz[1]/2]

          #Now, make it. If it is outside the width/height, we shift it into range
          diff = center[0]-rm_sz[0]/2
          if diff < 0
            center[0] -= diff
          end
          diff = center[0]+rm_sz[0]/2-@map.width 
          if diff > 0
            center[0] -= diff
          end
          diff = center[1]-rm_sz[1]/2
          if diff < 0
            center[1] -= diff
          end
          diff = center[1]+rm_sz[1]/2-@map.height
          if diff > 0
            center[1] -= diff
          end
          rm = add_room(rect, center[0]-rm_sz[0]/2, center[1]-rm_sz[1]/2, center[0]+rm_sz[0]/2, center[1]+rm_sz[1]/2)
        end


        #We have our initial room set, but they mostly overlap.
        #To separate them, we consider each rectangle in a row, and move it away from the area of greatest density by 1x1 pixel
        #  (with some randomness, of course). We continue until we pass the maximum number of cycles, or until we reach a phase with
        #  zero overlap.
        #We allow rooms to shuffle ~5 tiles past the boundary of that rectangle (but not outside the map)
        bigRect = RoomGroup.new(rect.lx-5, rect.ly-5, rect.hx+5, rect.hy+5)
        bigRect.lx = 1 if bigRect.lx<1
        bigRect.ly = 1 if bigRect.ly<1
        bigRect.hx = @map.width-2 if bigRect.hx>@map.width-2
        bigRect.hy = @map.height-2 if bigRect.hy>@map.height-2
        for i in 1..50  #No more than 50 cycles.
          allPass = true
          for rm in rect.rooms
            #First, shuffle 1px away from the edge if this room is past the edge of the bigRect.
            if rm.lx<bigRect.lx
              rm.lx += 1
              rm.hx += 1
              allPass = false
            end
            if rm.hx>bigRect.hx
              rm.lx -= 1
              rm.hx -= 1
              allPass = false
            end
            if rm.ly<bigRect.ly
              rm.ly += 1
              rm.hy += 1
              allPass = false
            end
            if rm.hy>bigRect.hy
              rm.ly -= 1
              rm.hy -= 1
              allPass = false
            end

            #Now compare to overlaps with other rooms
            for other in rect.rooms
              next if other == rm #Skip self

              #We consider +1px horiz and +1px vert for rooms to allow room for walls, etc.
              pass1 = (other.lx>rm.hx+1) || (other.hx<rm.lx-1) || (other.ly>rm.hy) || (other.hy<rm.ly)
              pass2 = (other.lx>rm.hx) || (other.hx<rm.lx) || (other.ly>rm.hy+1) || (other.hy<rm.ly-1)
              next if pass1 && pass2  #Skip non-overlapping

              #At this point we've got a collision
              allPass = false

              #We want to shuffle this room away from the other room *most* of the time.
              #We weight as follows, excluding any moves that would leave us out-of-bounds.
              # primary component (max-of-abs(N,S,E,W)) = 10x
              # secondary component (max-of-abs-minus-1(N,S,E,W)) = 8x
              # tertiary component (max-of-abs-minus-2(N,S,E,W)) = 2x
              # quarternary component (max-of-abs-minus-3(N,S,E,W)) = 1x
              gravity = [((rm.hx-rm.lx)-(other.hx-other.lx)) ,  ((rm.hy-rm.ly)-(other.hy-other.ly)) ]
              directions = []
              if gravity[0].abs>gravity[1].abs
                if gravity[0]>0
                  directions.push "E"
                else
                  directions.push "W"
                end
                if gravity[1]>0
                  directions.push "S"
                else
                  directions.push "N"
                end
              else
                if gravity[1]>0
                  directions.push "S"
                else
                  directions.push "N"
                end
                if gravity[0]>0
                  directions.push "E"
                else
                  directions.push "W"
                end
              end
              if directions[0] == "E"
                directions.push "W"
              elsif directions[0] == "W"
                directions.push "E"
              elsif directions[0] == "N"
                directions.push "S"
              elsif directions[0] == "S"
                directions.push "N"
              end
              if directions[1] == "E"
                directions.push "W"
              elsif directions[1] == "W"
                directions.push "E"
              elsif directions[1] == "N"
                directions.push "S"
              elsif directions[1] == "S"
                directions.push "N"
              end

              #For each direction, can we move that way?
              move_dir = [] #Potential move directions
              mult = 10
              for dir in directions
                next if dir=="W" && rm.lx<=bigRect.lx
                next if dir=="E" && rm.hx>=bigRect.hx
                next if dir=="N" && rm.ly<=bigRect.ly
                next if dir=="S" && rm.hy>=bigRect.hy
                for j in 1..mult
                  move_dir.push dir
                end
                if mult == 10
                  mult = 8
                elsif mult == 8
                  mult = 2
                else
                  mult = 1
                end
              end

              #Now, pick one and move in that direction
              next if move_dir.length==0
              dir = sample(move_dir)
              if dir == "E"
                rm.lx += 1
                rm.hx += 1
              elsif dir == "W"
                rm.lx -= 1
                rm.hx -= 1
              elsif dir == "S"
                rm.ly += 1
                rm.hy += 1
              elsif dir == "N"
                rm.ly -= 1
                rm.hy -= 1
              end
            end
          end
          if allPass
            #puts "Broke out early, after #{i} rounds"
            break
          end

          #If we have reached the last tick, we can try consolidating to a larger room
          if i == 50
            #puts "Rooms can't move; making MEGA room!"
            for i in 1..50  #Shouldn't ever reach 50
              for rm in rect.rooms
                mergeWith = nil
                for other in rect.rooms
                  next if other == rm #Skip self
                  next if (other.lx>rm.hx) || (other.hx<rm.lx) || (other.ly>rm.hy) || (other.hy<rm.ly)  #Skip non-overlapping
                  mergeWith = other
                  break
                end
                next unless mergeWith

                #We've got a merge
                #puts "   Merging two rooms..."
                add_room(rect, [rm.lx, mergeWith.lx].min, [rm.ly, mergeWith.ly].min, [rm.hx, mergeWith.hx].max, [rm.hy, mergeWith.hy].max)
                rect.rooms.delete mergeWith
                rect.rooms.delete rm
                break #Start iterating over.
              end
            end
            break
          end
        end
      }


      #At this point, we're guaranteed to not have rooms intersecting within their own rect.
      #However, they may collide between rects. To fix this, we simply shrink them "away"
      # from the other rect until they no longer overlap. 
      #This has the same effect as bounding them by their rects, but allows interesting non-harmful overlap.
      #Because we are shrinking them, they most certainly cannot generate new collisions within their rects.
      for i in 1..50  #No more than 50 cycles.
        allPass = true
        @room_groups.each {|rect|
          rect.rooms.each {|rm|
            #Compare to rmOther in rectOther
            @room_groups.each {|rectOther|
              #next if rectOther == rect  #No. We actually want to enforce a 1px shrink here.
              rectOther.rooms.each {|rmOther|
                next if rmOther==rm

                #We consider +1px horiz and +1px vert for rooms to allow room for walls, etc.
                pass1 = (rmOther.lx>rm.hx+1) || (rmOther.hx<rm.lx-1) || (rmOther.ly>rm.hy) || (rmOther.hy<rm.ly)
                pass2 = (rmOther.lx>rm.hx) || (rmOther.hx<rm.lx) || (rmOther.ly>rm.hy+1) || (rmOther.hy<rm.ly-1)
                next if pass1 && pass2  #Skip non-overlapping

                allPass = false #We've definitely got a collision at this point.

                #We need to see if we extend into the current RECT (not room). 
                #Rects are complex, but non-overlapping, so we can check line intersections pretty easily and 
                #  then simply shrink left/right/up/down 1px for this cycle.
                #puts "Forcing room shrink"
              
                #Do we extend too far down into this rect?
                if rm.hy+1 > rect.hy
                  rm.hy -= 1
                end

                #How about too far up?
                if rm.ly-1 < rect.ly
                  rm.ly += 1
                end

                #Too far right?
                if rm.hx+1 > rect.hx
                  rm.hx -= 1
                end

                #Too far left?
                if rm.lx-1 < rect.lx
                  rm.lx += 1
                end

                #Now that we've checked rect bounds, check the rm bounds too.

                #Do we extend from the right?
                if (rm.hx+1>=rmOther.lx) && ((rmOther.hy>=rm.ly-1) || (rmOther.ly<=rm.hy+1))
                  rm.hx -= 1
                end

                #Do we extend from the left?
                if (rm.lx-1<=rmOther.hx) && ((rmOther.hy>=rm.ly-1) || (rmOther.ly<=rm.hy+1))
                  rm.lx += 1
                end

                #Do we extend from the bottom?
                if (rm.hy+1>=rmOther.ly) && ((rmOther.hx>=rm.lx-1) || (rmOther.lx<=rm.hx+1))
                  rm.hy -= 1
                end

                #Do we extend from the top?
                if (rm.ly-1<=rmOther.hy) && ((rmOther.hx>=rm.lx-1) || (rmOther.lx<=rm.hx+1))
                  rm.ly += 1
                end     
              }
            }
          }
        }

        if allPass
          puts "Broke mega-shrink early after #{i} rounds"
          break
        end
      end

      #Finally, we absolutely must remove rooms that are 1 pixel in width/height (we'll actually remove 2 pixels too).
      #NOTE: This also removes rooms that somehow have hx<lx, but these would probably have a very small width anyway (so I think it's ok).
      for i in 1..50  #No more than 50 cycles.
        allPass = true
        @room_groups.each {|rect|
          rect.rooms.each {|rm|
            pass1 = (rm.hx-rm.lx)>1
            pass2 = (rm.hy-rm.ly)>1
            next if pass1 && pass2

            #Remove it, start over
            #puts "Removing very thin room: #{rm.lx} , #{rm.ly} => #{rm.hx} , #{rm.hy}"
            allPass = false
            rect.rooms.delete rm
            break
          }
        }
        if allPass
          puts "Broke final removal early after #{i} rounds"
          break
        end
      end

      #TODO: At this point, we might want to throw out the current results if we have absolutely no rooms left, 
      #      or possibly even if we have an empty rect. This is super-unlikely, but we might want to consider it.

      # Safety; warn if we're past the map boundary (but keep going).
      @room_groups.each {|rect|
        for rm in rect.rooms
          unless @map.valid_tile?(rm.lx-1,rm.ly-1) && @map.valid_tile?(rm.hx+1,rm.hy+1)
            puts "ROOM EXTENDED TOO FAR: #{rm.lx},#{rm.ly} => #{rm.hx},#{rm.hy} w.r.t. #{@map.width},#{@map.height}"
            rm.lx = [rm.lx, 1].max
            rm.hx = [rm.hx, @map.width-2].min
            rm.ly = [rm.ly, 1].max
            rm.hy = [rm.hy, @map.height-2].min
          end
        end
      }

      # We now need to color in each room's tiles, in order for later algorithms to function correctly.
      @room_groups.each {|rect|
        #puts "Printing rect..."
        for rm in rect.rooms
          #puts "   Printing rm #{rm.lx},#{rm.ly} => #{rm.hx},#{rm.hy} :: #{@map.width} , #{@map.height}"
          for x in rm.lx..rm.hx
            for y in rm.ly..rm.hy
              if @map.valid_tile? x,y #Shouldn't be needed, but we sometimes accidentally overlap.
                puts "ROOM OVERLAP" if @map.data[x][y][0]==Tile::Room
                @map.data[x][y][0] = Tile::Room
              else 
                puts "BOGUS ERROR!"
              end
            end
          end
        end
      }
    end


    # This function walks through several heurstical approaches to connecting rooms:
    # First, we connect rooms that are only off by 1 pixel. Next, we connect triplets
    # of rooms that form nice wide triangles, using a bridge-walking algorithm.
    # After that, we connect rooms that are mostly surrounded by the bridges.
    # Next, we connect rooms directly, via L-connectors, and via lightning-connectors,
    # without allowing connections that would intersect (or even come near) other rooms.
    # Finally, if any rooms are left, we throw up our hands and add teleporters. 
    def generate_connections()
      #Keep a record of room<->room (either direction works)
      @connections = [] #(a->b)
      blacklist = [] #(a->b)

      #Phase 1: With 80% chance, connect pairs of rooms that are only 1 px apart.
      banned_pairs = [] #(a->b); this is the output of phase 1. We specifically avoid connecting these later.
      @room_groups.each {|rect|
        rect.rooms.each {|rm|
          @room_groups.each {|otherRect|
            otherRect.rooms.each {|otherRm|
              next if rm==otherRm
              next if connects? rm, otherRm
              next if in_bidilist? blacklist, rm, otherRm
              toAdd = nil   #Which point to color, if we pass the last attempt

              #Other is N?
              if otherRm.hy == rm.ly-2
                #Pick a random single dot.
                lx = [rm.lx, otherRm.lx].max
                hx = [rm.hx, otherRm.hx].min
                wid = hx-lx-1
                if wid>=1
                  toAdd = [lx+uniform(1,wid), rm.ly-1]
                end
              end

              #Other is S?
              if otherRm.ly == rm.hy+2
                #Pick a random single dot.
                lx = [rm.lx, otherRm.lx].max
                hx = [rm.hx, otherRm.hx].min
                wid = hx-lx-1
                if wid>=1
                  toAdd = [lx+uniform(1,wid),rm.hy+1]
                end
              end

              #Other is W?
              if otherRm.hx == rm.lx-2
                #Pick a random single dot.
                ly = [rm.ly, otherRm.ly].max
                hy = [rm.hy, otherRm.hy].min
                wid = hy-ly-1
                if wid>=1
                  toAdd = [rm.lx-1,ly+uniform(1,wid)]
                end
              end

              #Other is E?
              if otherRm.lx == rm.hx+2
                #Pick a random single dot.
                ly = [rm.ly, otherRm.ly].max
                hy = [rm.hy, otherRm.hy].min
                wid = hy-ly-1
                if wid>=1
                  toAdd = [rm.hx+1,ly+uniform(1,wid)]
                end
              end

              next unless toAdd

              #we blacklist after one attempt
              if @@my_rand.rand >0.8 
                blacklist.push([rm,otherRm])
                banned_pairs.push([rm,otherRm])
                next
              end

              #For now, just color it.
              @map.data[toAdd[0]][toAdd[1]][0] = Tile::Bridge
              @map.data[toAdd[0]+1][toAdd[1]][0] = Tile::Door if @map.is_tile? toAdd[0]+1, toAdd[1], Tile::Room
              @map.data[toAdd[0]-1][toAdd[1]][0] = Tile::Door if @map.is_tile? toAdd[0]-1, toAdd[1], Tile::Room
              @map.data[toAdd[0]][toAdd[1]+1][0] = Tile::Door if @map.is_tile? toAdd[0], toAdd[1]+1, Tile::Room
              @map.data[toAdd[0]][toAdd[1]-1][0] = Tile::Door if @map.is_tile? toAdd[0], toAdd[1]-1, Tile::Room

              #Save it
              @connections.push([rm,otherRm])
            }
          }
        }
      }

      #Phase 2: We now try to find triplets of rooms that form nice "thick" triangles, and make Y-connectors between them with 50% chance.
      blacklist = [] # (A->B->C)
      @room_groups.each {|rectA|
        rectA.rooms.each {|rmA|
          @room_groups.each {|rectB|
            rectB.rooms.each {|rmB|
              next if rmA == rmB
              @room_groups.each {|rectC|
                rectC.rooms.each {|rmC|
                  next if rmA == rmC
                  next if rmB == rmC
                  next if in_tridilist? blacklist, rmA, rmB, rmC

                  #A,B,C are different; see what kind of triangle they form.
                  tri = compute_triangle(rmA, rmB, rmC)
                  next unless tri #Only happens if it's a line, not a triangle.

                  #See how big the difference is.
                  biggestDiff = [(tri.angle1-tri.angle2).abs, (tri.angle2-tri.angle3).abs, (tri.angle1-tri.angle3).abs].max
                  next if biggestDiff > 20  #Some random guess.

                  next if @map.is_tile? tri.centerX, tri.centerY, Tile::Room

                  #Now calculate the rect for potential "empty space" by growing from the center.
                  spaceRect = RoomGroup.new tri.centerX , tri.centerY , tri.centerX , tri.centerY

                  #Grow no more than 10 tiles in each direction
                  for i in 1..10
                    failed = 0

                    #Grow N?
                    spaceRect.ly -= 1
                    if spaceRect.ly<0 || @map.has_tile?(spaceRect, Tile::Room)
                      spaceRect.ly += 1
                      failed += 1
                    end

                    #Grow S?
                    spaceRect.hy += 1
                    if spaceRect.hy>@map.height || @map.has_tile?(spaceRect, Tile::Room)
                      spaceRect.hy -= 1
                      failed += 1
                    end
                  
                    #Grow W?
                    spaceRect.lx -= 1
                    if spaceRect.lx<0 || @map.has_tile?(spaceRect, Tile::Room)
                      spaceRect.lx += 1
                      failed += 1
                    end

                    #Grow E?
                    spaceRect.hx += 1
                    if spaceRect.hx>@map.width || @map.has_tile?(spaceRect, Tile::Room)
                      spaceRect.hx -= 1
                      failed += 1
                    end

                    break if failed == 4                  
                  end

                  #Shrink 1 px in each direction, then cancel if too small
                  spaceRect.lx += 1
                  spaceRect.ly += 1
                  spaceRect.hx -= 1
                  spaceRect.hy -= 1
                  next if spaceRect.hx-spaceRect.lx < 2
                  next if spaceRect.hy-spaceRect.ly < 2

                  #Save it
                  blacklist.push [rmA, rmB, rmC]

                  #Draw the "grow" areas as Pits; we use this later for a few things (including drawing).
                  for x in spaceRect.lx..spaceRect.hx
                    for y in spaceRect.ly..spaceRect.hy
                      if !@map.is_tile?(x,y,Tile::Room) && !@map.is_tile?(x,y,Tile::Bridge) #Don't overwrite existing paths.
                        @map.data[x][y][0] = Tile::Pit
                      end
                    end
                  end

                  #Pick a random tile to serve as the center.
                  cent = [uniform(spaceRect.lx, spaceRect.hx), uniform(spaceRect.ly, spaceRect.hy)]

                  #Now, generate paths to all 3 rooms from the center.
                  generate_tri_paths(rmA, rmB, rmC, cent)
                }
              }
            }
          }
        }
      }

      #Phase 3.1: Build a lookup for red tiles.
      red_list = {} #[x,y]=>[rmA,rmB,rmC] (which rooms a given red tile connects to).
      blacklist = {} #[x,y]=>1 for rooms (Tile::Door tiles). Just for safety
      room_rooms = {} #room=>[room,] array that it's in (in red_list)
      @map.teleports = []  #(room,room) pairs that will need bi-directional teleporters.
      for x in 0..@map.width
        for y in 0..@map.height
          next unless @map.is_tile? x,y,Tile::Bridge
          next if red_list.include? [x,y] #Already found it.

          #Build recursively, through Tile::Bridge and Tile::Door. Store in the same shared arrray.
          rooms = [] #A,B,C; shared between all tiles in this red list.
          to_scan = [[x,y]]

          #puts "Building outwards from #{x} , #{y}"
          while !to_scan.empty?
            #Mark new red tiles.
            tile = to_scan.shift
            #puts "  Found tile #{tile[0]} , #{tile[1]}" if @map.data[tile[0]][tile[1]][0] == Tile::Bridge
            red_list[tile] = rooms  if @map.is_tile? tile[0],tile[1],Tile::Bridge

            #Mark new pink tiles (doors)
            if @map.is_tile? tile[0],tile[1],Tile::Door
              #We use the blacklist to prevent room (Tile::Door) cycles.
              blacklist[tile] = 1

              #Find out which room has it.
              @room_groups.each {|rect|
                rect.rooms.each {|rm|
                  if tile[0]>=rm.lx && tile[1]>=rm.ly && tile[0]<=rm.hx && tile[1]<=rm.hy
                    #puts "  Found new room" unless rooms.include? rm
                    rooms.push rm unless rooms.include? rm
                    room_rooms[rm] = rooms unless room_rooms.include? rm

                    #Now, add every pink tile in this room to the exploration list.
                    for rmX in rm.lx..rm.hx
                      for rmY in rm.ly..rm.hy
                        next unless @map.data[rmX][rmY][0] == Tile::Door
                        next if blacklist.include? [rmX,rmY]
                        #puts "  Room added child rooms!"
                        to_scan.push([rmX,rmY])
                      end
                    end
                  end
                }
              }
            end

            #Mark neighbors for exploration
            for i in 1..4
              next_tile = [tile[0], tile[1]]
              next_tile[0] += 1 if i==1
              next_tile[0] -= 1 if i==2
              next_tile[1] += 1 if i==3
              next_tile[1] -= 1 if i==4
              next if red_list.include?(next_tile) || to_scan.include?(next_tile) || blacklist.include?(next_tile)
              next if next_tile[0]<0 || next_tile[0]>@map.width || next_tile[1]<0 || next_tile[1]>@map.height
              to_scan.push(next_tile) if @map.is_tile? next_tile[0],next_tile[1],[Tile::Bridge,Tile::Door]
            end
          
          end
        end
      end
      #puts "Found #{red_list.length} connected red zones"
      #puts "Found #{room_rooms.length} connected room zones"
      blacklist = {} #room_list->1, for printing.
      for reds,rooms in red_list
        next if blacklist.include? rooms
        blacklist[rooms] = 1
      end
      for room,rooms in room_rooms
        next if blacklist.include? rooms
        blacklist[rooms] = 1
      end
      #puts "Found #{blacklist.length} room lists:"
      for rooms,one in blacklist
        room_str = ""
        for rm in rooms
          room_str += " , " unless room_str.empty?
          room_str += "[#{rm.lx},#{rm.ly}=>#{rm.hx},#{rm.hy}]"
        end
        #puts "  Room list: #{room_str}"
      end

      #Phase 3.2: Rooms that have red paths touching will randomly connect to them. 
      @room_groups.each {|rect|
        rect.rooms.each {|rm|
          #We count the number of red tiles that are 1 tile away which this room is not already connected to.
          red_options = {}
          cands = []
          for x in rm.lx+1..rm.hx-1
            cands.push [x,rm.ly-1]
            cands.push [x,rm.hy+1]
          end
          for y in rm.ly+1..rm.hy-1
            cands.push [rm.lx-1,y]
            cands.push [rm.hx+1,y]
          end
          for cand in cands
            next unless @map.valid_tile? cand[0] , cand[1]
            next unless @map.is_tile? cand[0],cand[1],Tile::Bridge
            next if red_list.include?(cand) && red_list[cand].include?(rm)
            #puts "NEW_Candidate: #{cand[0]} , #{cand[1]}"
            #puts "  ; count is: #{red_list[cand].length}" if red_list.has_key?(cand)
            red_options[cand] = 1
          end
          next if red_options.empty?

          #Compare to the total perimeter.
          perim = ((rm.hx-rm.lx) + (rm.hy-rm.ly) + 1)*2
          #puts "Perimeter check on room with #{red_options.length} red options and perimeter of #{perim}"

          #Always connect the room if more than 10% of the perimeter touches.
          if uniform(1,red_options.length) >= perim/10
            to_connect = sample(red_options.keys)
            #puts "Perimeter-connecting room at: #{to_connect}"
            for i in 1..4
              cand = [to_connect[0],to_connect[1]]
              cand[0] += 1 if i==1
              cand[0] -= 1 if i==2
              cand[1] += 1 if i==3
              cand[1] -= 1 if i==4
              if cand[0]>=rm.lx && cand[0]<=rm.hx && cand[1]>=rm.ly && cand[1]<=rm.hy
                @map.data[cand[0]][cand[1]][0] = Tile::Door

                #Hook it up to our path search algorithm.
                room_rooms[rm] = [rm] unless room_rooms.include? rm
                super_rooms = combine_rooms(red_list, room_rooms, room_rooms, red_list[cand])  #TODO: Relatively untested...
              end
            end
            red_list[to_connect].push rm
          end
        }
      }

      #Phase 3.3: We want room_rooms to eventually contain a single entry with every single room.
      #To do this, we first find rooms with absolutely no outgoing connections and try to make
      # "obvious" connections (i.e., straight line, no other rooms in the way).
      puts "Connecting solo rooms..."
      to_do = []
      for i in 1..1  #NOTE: Only 1 try makes sense; we will be included in room_rooms after that.
        @room_groups.each {|rect|
          rect.rooms.each {|rm|
            next if room_rooms.include? rm
            to_do.push rm
            room_rooms[rm] = [rm]
          }
        }
      end
      if room_rooms.values.uniq.length>1
        to_do.each {|rm|
          break if room_rooms.values.uniq.length==1
          connect_to_network(rm, red_list, room_rooms, banned_pairs)
        }
      end

      #Phase 3.4: Try to connect rooms randomly.
      puts "Connecting random rooms..."
      for i in 1..2  #Give it two tries; the first one might jiggle a few more paths into place.
        @room_groups.each {|rect|
          rect.rooms.each {|rm|
            break if room_rooms.values.uniq.length==1
            connect_to_network(rm, red_list, room_rooms, banned_pairs)
          }
        }
      end

      #Phase 3.5: Add teleporters; the rest are hopeless.
      #puts "Connecting hopeless rooms..."
      @room_groups.each {|rect|
        shuffle(rect.rooms).each {|rm|
          break if room_rooms.values.uniq.length==1

          #Find a room that's not connected to this.
          goodRm = nil
          @room_groups.each {|otherRect|
            shuffle(otherRect.rooms).each {|otherRm|
              next if otherRm==rm
              next if room_rooms[rm].include? otherRm
              next if @map.teleports.include?([rm, otherRm]) || @map.teleports.include?([otherRm, rm]) #Really shouldn't happen
              goodRm = otherRm
              break
            }
          }
          if goodRm
            #puts "Giving up; adding teleporter"
            @map.teleports.push [rm, goodRm]
            combine_rooms(red_list, room_rooms, room_rooms[rm], room_rooms[goodRm])
          else
            puts "ERROR: Unable to find teleporter for room."
          end
        }
      }
    end

    #This function takes a room and connects it to one of its non-peers (peers being defined in room_rooms).
    #It then merges its peers with the peers of the room it connnected to. 
    #Rooms are merged by considering several candidates, and choosing ones that are ranked by how "good" they look.
    def connect_to_network(rm, red_list, room_rooms, banned_pairs)
      #We build up a list of candidate room connections.
      candidates = {}  #rooms

      #We first consider rooms that are <=20 tiles away on the 25/50/75 lines in each direction.
      #We stop when we reach a pit or a room.
      blacks = {}  #[x,y]=>1 for first black/pink square (i.e., room)
      for dir in 1..4 #Direction we are checking
        for offset in 1..3 #X/4 of room we are checking.
          for pos in 1..20 #How many tiles out
            #Check this tile, stop if we have something.
            if dir==1     #N
              tile = [rm.lx+offset*(rm.hx-rm.lx)/4, rm.ly-pos]
            elsif dir==2  #S
              tile = [rm.lx+offset*(rm.hx-rm.lx)/4, rm.ly+pos]
            elsif dir==3  #W
              tile = [rm.lx-pos, rm.ly+offset*(rm.hy-rm.ly)/4]
            elsif dir==4  #E
              tile = [rm.lx+pos, rm.ly+offset*(rm.hy-rm.ly)/4]
            end
            break unless @map.valid_tile? tile[0],tile[1]
            break if @map.is_tile? tile[0],tile[1],Tile::Pit  #Don't cross pits.
            if @map.is_tile? tile[0],tile[1],[Tile::Room,Tile::Door]
              blacks[tile] = 1
              break
            end
          end
        end
      end

      #Convert each black tile to the room that contains it.
      @room_groups.each {|otherRect|
        otherRect.rooms.each {|otherRm|
          next if otherRm == rm
          next if banned_pairs.include? [rm, otherRm]
          next if banned_pairs.include? [otherRm, rm]
          next if room_rooms[otherRm] && room_rooms[otherRm].include?(rm)
          for pt,one in blacks
            if pt[0]>=otherRm.lx && pt[0]<=otherRm.hx && pt[1]>=otherRm.ly && pt[1]<=otherRm.hy
              candidates[otherRm] = 1
              break
            end
          end
        }
      }

      #Our second set of tiles to check are those within a certain distance of the current room.
      #We basically use a distance check; are the two rooms' circles (defined by the average of w/h)
      # more than 15 pixels apart?
      @room_groups.each {|otherRect|
        otherRect.rooms.each {|otherRm|
          next if otherRm == rm
          next if banned_pairs.include? [rm, otherRm]
          next if banned_pairs.include? [otherRm, rm]
          next if room_rooms[otherRm] && room_rooms[otherRm].include?(rm)
          rm1rad = ((rm.hx-rm.lx)/2 + (rm.hy-rm.ly)/2)/2
          rm2rad = ((otherRm.hx-otherRm.lx)/2 + (otherRm.hy-otherRm.ly)/2)/2
          rmDist = dist([rm.lx+(rm.hx-rm.lx)/2,rm.ly+(rm.hy-rm.ly)/2] , [otherRect.lx+(otherRect.hx-otherRect.lx)/2,otherRect.ly+(otherRect.hy-otherRect.ly)/2])
          next if rmDist-rm1rad-rm2rad > 15
          candidates[otherRm] = 1
        }
      }

      #Now we have a set of candidates; we turns these into a set of candidate paths.
      #Each room will try a set of "obvious" candidate paths (for example, straight line, L bend, lightning bolt).
      #If these intersect another room, they will have a "bad" count equal to the number of black/pink squares they
      #over-ride. They will have a "warn" count equal to the number of tiles on the path that are directly next to a 
      #black square. Ideally, we choose a path (randomly) with no warn/bad count. Else, pick the one with the least
      #count (and failing that, the least bad count).
      paths = {}            #[red_tile_path] => [warn,bad]
      min_vals = [999,999]  #min_warn,min_bad; where min_warn has no bad tiles
      num_rands = 3  #How many random candidates to make (TODO: 3)
      for cand,one in candidates
        break if room_rooms.values.uniq.length==1
        hasDirectLine = false

        #First, check if they overlay horizontally.
        startX = [rm.lx,cand.lx].max
        endX = [rm.hx,cand.hx].min
        if endX-startX >= 2
          hasDirectLine = true
          #Which horizontal line is closer?
          if (rm.ly-cand.hy).abs < (rm.hy-cand.ly).abs
            myY = rm.ly
            dir = -1  #N
          else
            myY = rm.hy
            dir = 1   #S
          end

          #Now, create 3 paths (random X) that move straight towards that room.
          for offset in 1..num_rands
            path = []
            warnBad = [0,0] #warn/bad
            currPt = [uniform(startX+1,endX-1),myY]
            offsets = [0,dir]
            while true
              break if build_out(path, currPt, offsets, warnBad, cand)
            end

            #Path done.
            min_vals[0] = [min_vals[0],warnBad[0]].min if warnBad[1]==0
            min_vals[1] = [min_vals[1],warnBad[1]].min
            paths[path] = warnBad
          end
        end

        #Next, check if they overlay vertically.
        startY = [rm.ly,cand.ly].max
        endY = [rm.hy,cand.hy].min
        if endY-startY >= 2
          hasDirectLine = true
          #Which vertical line is closer?
          if (rm.lx-cand.hx).abs < (rm.hx-cand.lx).abs
            myX = rm.lx
            dir = -1  #W
          else
            myX = rm.hx
            dir = 1   #E
          end

          #Now, create 3 paths (randomY) that move straight towards that room.
          for offset in 1..num_rands
            path = []
            warnBad = [0,0] #warn/bad
            currPt = [myX, uniform(startY+1,endY-1)]
            offsets = [dir,0]
            while true
              break if build_out(path, currPt, offsets, warnBad, cand)
            end

            #Path done.
            min_vals[0] = [min_vals[0],warnBad[0]].min if warnBad[1]==0
            min_vals[1] = [min_vals[1],warnBad[1]].min
            paths[path] = warnBad
          end
        end

        #Next, try building a few angled L paths towards them. These exist (by definition) only if no straight-line paths exist.
        if !hasDirectLine
          #We always consider 3 random paths in each direction that matters.
          for offset in 1..num_rands
            #We consider several "orientations"...
            orient = [] #[startX,startY,buildAmt,buildDir,secondBuildDir],...

            #The first has the other room "left" of this one & we move right.
            if cand.lx<=rm.lx
              #Determine our build dirs.
              buildDir = "E"
              secondBuildDir = rm.hy>cand.hy ? "S" : "N"

              #Pick a random starting point on the right edge of the other room.
              next if cand.hy-cand.ly < 2
              currPt = [cand.hx,uniform(cand.ly+1,cand.hy-1)]

              #Decide how far to build out horizontally.
              hMin = [cand.hx+1, rm.lx+1].max - cand.hx
              hMax = [cand.hx+1, rm.hx-1].max - cand.hx
              next if hMax-hMin < 1
              hRun = uniform(hMin, hMax)

              #Save it
              orient.push [currPt[0], currPt[1], hRun, buildDir, secondBuildDir]
            end

            #The next has the room "right" of this room, and we move left.
            if cand.hx>=rm.hx
              #Determine our build dirs.
              buildDir = "W"
              secondBuildDir = rm.hy>cand.hy ? "S" : "N"

              #Pick a random starting point on the left edge of the other room.
              next if cand.hy-cand.ly < 2
              currPt = [cand.lx,uniform(cand.ly+1,cand.hy-1)]

              #Decide how far to build out horizontally.
              hMin = 1+ cand.lx - [cand.lx-1, rm.hx-1].min
              hMax = cand.lx - [cand.lx-1, rm.lx+1].min
              next if hMax-hMin < 1
              hRun = uniform(hMin, hMax)

              #Save it
              orient.push [currPt[0], currPt[1], hRun, buildDir, secondBuildDir]
            end

            #The third room has the other one above, and we build down.
            if cand.ly<=rm.ly
              #Determine our build dirs.
              buildDir = "S"
              secondBuildDir = rm.hx>cand.hx ? "E" : "W"

              #Pick a random starting point on the lower edge of the other room.
              next if cand.hx-cand.lx < 2
              currPt = [uniform(cand.lx+1,cand.hx-1), cand.hy]

              #Decide how far to build out vertically.
              hMin = [cand.hy+1, rm.ly+1].max - cand.hy
              hMax = [cand.hy+1, rm.hy-1].max - cand.hy
              next if hMax-hMin < 1
              hRun = uniform(hMin, hMax)

              #Save it
              orient.push [currPt[0], currPt[1], hRun, buildDir, secondBuildDir]
            end

            #The next has the other room below, and we move up.
            if cand.hy>=rm.hy
              #Determine our build dirs.
              buildDir = "N"
              secondBuildDir = rm.hx>cand.hx ? "E" : "W"

              #Pick a random starting point on the top edge of the other room.
              next if cand.hx-cand.lx < 2
              currPt = [uniform(cand.lx+1,cand.hx-1), cand.ly]

              #Decide how far to build out vertically.
              hMin = 1+ cand.ly - [cand.ly-1, rm.hy-1].min
              hMax = cand.ly - [cand.ly-1, rm.ly+1].min
              next if hMax-hMin < 1
              hRun = uniform(hMin, hMax)

              #Save it
              orient.push [currPt[0], currPt[1], hRun, buildDir, secondBuildDir]
            end


            #Now, actually build these
            for orie in orient
              currX,currY,buildAmt,buildDir,secondBuildDir = orie
              path = []
              currPt = [currX,currY]
              warnBad = [0,0] #warn,bad

              #First build dir
              offsets = [0,0]
              offsets[0] = 1 if buildDir=="E"
              offsets[0] = -1 if buildDir=="W"
              offsets[1] = 1 if buildDir=="S"
              offsets[1] = -1 if buildDir=="N"
              for b in 1..buildAmt
                break if build_out(path, currPt, offsets, warnBad, rm)
              end

              #Second build dir
              offsets = [0,0]
              offsets[0] = 1 if secondBuildDir=="E"
              offsets[0] = -1 if secondBuildDir=="W"
              offsets[1] = 1 if secondBuildDir=="S"
              offsets[1] = -1 if secondBuildDir=="N"
              while true
                break if build_out(path, currPt, offsets, warnBad, rm)
              end

              #Path done.
              min_vals[0] = [min_vals[0],warnBad[0]].min if warnBad[1]==0
              min_vals[1] = [min_vals[1],warnBad[1]].min
              paths[path] = warnBad
            end

          end
        end

        #Next, try building a few snaking lightning-bolt paths towards them. Like the L brackets, these are only valid if the rooms don't directly line up.
        if !hasDirectLine
          #We always consider 3 random paths in each direction that matters.
          for offset in 1..num_rands
            #We consider several "orientations"...
            orient = [] #[startX,startY,endX,endY,buildDir(from_start),buildDist(between start/end)],... start is cand, end is rm
            minZigZag = 5 #How many spaces we need to draw our zig-zag

            #The first has the other room "left" of this and far below/above. We move up/down, then join at the center.
            if cand.lx<=rm.lx
              #Determine our build dir; make sure we have enough space to build.
              buildDir = rm.hy>cand.hy ? "S" : "N"
              buildDist = buildDir=="S" ? rm.ly-cand.hy : cand.ly-rm.hy
              next if buildDist<minZigZag

              #We need to adjust the start range for the room's zigzag line.
              midX = [cand.hx,rm.lx].max

              #Pick a random starting point on the top/bottom edge of the room.
              next if cand.hx-cand.lx < 2
              next if rm.hx-midX < 2
              startPt = [uniform(cand.lx+1,cand.hx-1), (buildDir=="S" ? cand.hy : cand.ly)]
              endPt = [uniform(midX+1,rm.hx-1), (buildDir=="N" ? rm.hy : rm.ly)]

              #Save it
              orient.push [startPt[0], startPt[1], endPt[0], endPt[1], buildDir,buildDist]
            end

            #The second has the other room "right" of this, and far below/above. We move up/down, then join at the center.
            if cand.hx>=rm.hx
              #Determine our build dir; make sure we have enough space to build.
              buildDir = rm.hy>cand.hy ? "S" : "N"
              buildDist = buildDir=="S" ? rm.ly-cand.hy : cand.ly-rm.hy
              next if buildDist<minZigZag

              #We need to adjust the start range for the room's zigzag line.
              midX = [cand.lx,rm.hx].min

              #Pick a random starting point on the top/bottom edge of the room.
              next if cand.hx-cand.lx < 2
              next if midX-rm.lx < 2
              startPt = [uniform(cand.lx+1,cand.hx-1), (buildDir=="S" ? cand.hy : cand.ly)]
              endPt = [uniform(rm.lx+1,midX-1), (buildDir=="N" ? rm.hy : rm.ly)]

              #Save it
              orient.push [startPt[0], startPt[1], endPt[0], endPt[1], buildDir,buildDist]
            end

            #The third has the other room "above" of this and far left/right. We move left/right, then join at the center.
            if cand.ly<=rm.ly
              #Determine our build dir; make sure we have enough space to build.
              buildDir = rm.hx>cand.hx ? "E" : "W"
              buildDist = buildDir=="E" ? rm.lx-cand.hx : cand.lx-rm.hx
              next if buildDist<minZigZag

              #We need to adjust the start range for the room's zigzag line.
              midY = [cand.hy,rm.ly].max

              #Pick a random starting point on the left/right edge of the room.
              next if cand.hy-cand.ly < 2
              next if rm.hy-midY < 2
              startPt = [(buildDir=="E" ? cand.hx : cand.lx) , uniform(cand.ly+1,cand.hy-1)]
              endPt = [(buildDir=="W" ? rm.hx : rm.lx) , uniform(midY+1,rm.hy-1)]

              #Save it
              orient.push [startPt[0], startPt[1], endPt[0], endPt[1], buildDir,buildDist]
            end

            #The fourth has the other room "below" this, and far left/right. We move left/right, then join at the center.
            if cand.hy>=rm.hy
              #Determine our build dir; make sure we have enough space to build.
              buildDir = rm.hx>cand.hx ? "E" : "W"
              buildDist = buildDir=="E" ? rm.lx-cand.hx : cand.lx-rm.hx
              next if buildDist<minZigZag

              #We need to adjust the start range for the room's zigzag line.
              midY = [cand.ly,rm.hy].min

              #Pick a random starting point on the left/right edge of the room.
              next if cand.hy-cand.ly < 2
              next if midY-rm.ly < 2
              startPt = [(buildDir=="E" ? cand.hx : cand.lx) , uniform(cand.ly+1,cand.hy-1)]
              endPt = [(buildDir=="W" ? rm.hx : rm.lx) , uniform(rm.ly+1,midY-1)]

              #Save it
              orient.push [startPt[0], startPt[1], endPt[0], endPt[1], buildDir,buildDist]
            end


            #Now, actually build out those orientations
            for orie in orient
              startX,startY,endX,endY,buildDir,buildDist = orie
              path = []
              currPt = [startX,startY]
              warnBad = [0,0] #warn,bad

              #Compute the second walkdir
              if buildDir=="S" || buildDir=="N"
                buildDir2 = endX>startX ? "E" : "W"
                buildDist2 = (endX-startX).abs
              else
                buildDir2 = endY>startY ? "S" : "N"
                buildDist2 = (endY-startY).abs
              end

              #We have to build a random number of steps in the first direction (between 25 and 50%), but at least 2 (to avoid copious warnings)
              fwdSteps = uniform([2,buildDist/4].max,[2,buildDist/2].max)
              offsets = [0,0]
              offsets[0] = 1 if buildDir=="E"
              offsets[0] = -1 if buildDir=="W"
              offsets[1] = 1 if buildDir=="S"
              offsets[1] = -1 if buildDir=="N"
              for b in 1..fwdSteps
                break if build_out(path, currPt, offsets, warnBad, rm)
              end

              #Second build dir
              offsets = [0,0]
              offsets[0] = 1 if buildDir2=="E"
              offsets[0] = -1 if buildDir2=="W"
              offsets[1] = 1 if buildDir2=="S"
              offsets[1] = -1 if buildDir2=="N"
              for b in 1..buildDist2
                break if build_out(path, currPt, offsets, warnBad, rm)
              end

              #Now, walk to the goal
              offsets = [0,0]
              offsets[0] = 1 if buildDir=="E"
              offsets[0] = -1 if buildDir=="W"
              offsets[1] = 1 if buildDir=="S"
              offsets[1] = -1 if buildDir=="N"
              for b in 1..(buildDist-fwdSteps)
                break if build_out(path, currPt, offsets, warnBad, rm)
              end

              #Path done.
              min_vals[0] = [min_vals[0],warnBad[0]].min if warnBad[1]==0
              min_vals[1] = [min_vals[1],warnBad[1]].min
              paths[path] = warnBad
            end
          end
        end

        #Finally, pull out all paths with the lowest rank and pick a random one.
        #Draw it and save its path information.
        #NOTE: Don't forget that "999" actually means "no value"... in this case there should also be no entries?
        minWarn = min_vals[0]
        minBad = min_vals[1]
        #NOTE: We don't actually use these right now, and they might not actually be computed correctly!
        goodPaths = []  #[tiles],...
        for tiles,warnBad in paths
          next if warnBad[0]>0 || warnBad[1]>0
          goodPaths.push tiles
        end

       #Select a path, color it (and merge the rooms.
        if !goodPaths.empty?
          #Pick a random path.
          path = sample(goodPaths)

          #The first thing we need to do is combine this room's room list with that of the room we've merged with.
          #We combine these into a "super" room.
          orig_rooms = room_rooms[rm]  #based off "this" room
          other_rooms = nil
          for tile in [path[0],path[-1]]  #We make no assumptions about whether the first or last tile represents THIS room.
            @room_groups.each {|otherRect| #TODO: This only works because we DON'T use rooms with a warning level!
              otherRect.rooms.each {|otherRm|
                for i in 1..4
                  cand = [tile[0],tile[1]]
                  cand[0] += 1 if i==1
                  cand[0] -= 1 if i==2
                  cand[1] += 1 if i==3
                  cand[1] -= 1 if i==4
                  if cand[0]>=otherRm.lx && cand[0]<=otherRm.hx && cand[1]>=otherRm.ly && cand[1]<=otherRm.hy
                    #Color either way
                    @map.data[cand[0]][cand[1]][0] = Tile::Door
                    if otherRm != rm
                      other_rooms = room_rooms[otherRm]
                    end
                  end
                end
              }
            }
          end

          #The other room really shouldn't include this one.
          if other_rooms.nil?
            puts "=================================================="
            puts "SUPER AMAZING ERROR: OTHER ROOM IS NIL."
            puts "=================================================="
          end
          if other_rooms && other_rooms.include?(rm)
            #TODO: This only triggers under certain conditions. I kind of like the additional randomness however... 
            #      so for now it's harmless (we want a few cycles anyway).
            #puts "=================================================="
            #puts "SUPER AMAZING ERROR: OTHER ROOM INCLUDES THIS ONE."
            #puts "=================================================="
          end

          #Combine them
          super_rooms = combine_rooms(red_list, room_rooms, orig_rooms, other_rooms)

          #Color the path.
          for tile in path
            @map.data[tile[0]][tile[1]][0] = Tile::Tunnel
            red_list[tile] = super_rooms unless red_list.include? tile  #New tiles always use the super room.
          end
        end
      end

    end


    #Combine two rooms lists, and update both red_list and room_rooms
    def combine_rooms(red_list, room_rooms, orig_rooms, other_rooms)
      #Either way, we still combine them.
      super_rooms = []
      for rm in orig_rooms
        super_rooms.push rm
      end
      if other_rooms
        for rm in other_rooms
          super_rooms.push rm unless super_rooms.include? rm  #Shouldn't overlap...
        end
      end

      #Now we need to update anything that referenced the old rooms
      red_list.each { |tile,rooms| 
        red_list[tile] = super_rooms if rooms==orig_rooms || rooms==other_rooms
      }
      room_rooms.each { |rm,rooms| 
        room_rooms[rm] = super_rooms if rooms==orig_rooms || rooms==other_rooms
      }

      return super_rooms
    end


    #Build an [x,y] path outwards by [offX,offY], updating warn/bad counts. Returns true if we are "done"
    def build_out(path, currPt, offsets, warnBad, endRoom)
      #An existing point can never be counted towards its own blacklist.
      okPts = [[currPt[0],currPt[1]]]

      #...in addition, the end point can never be counted. This will (almost) always be 
      #   +2 in the direciton we are traveling; we won't catch bends in the path this way,
      #   but those will almost always trigger other warnings anyway.
      candOk = [currPt[0]+2*offsets[0],currPt[1]+2*offsets[1]]
      if candOk[0]>=endRoom.lx && candOk[0]<=endRoom.hx && candOk[1]>=endRoom.ly && candOk[1]<=endRoom.hy
        okPts.push candOk
      end

      #Update
      currPt[0] += offsets[0]
      currPt[1] += offsets[1]

      #Safeguard
      if !@map.valid_tile? currPt[0] , currPt[1]
        puts "Bad safeguard triggered; setting to 99"
        warnBad[1] = 99
        return true
      end

      #Done?
      if currPt[0]>=endRoom.lx && currPt[0]<=endRoom.hx && currPt[1]>=endRoom.ly && currPt[1]<=endRoom.hy
        return true
      end

      #Bad?
      if @map.is_tile? currPt[0],currPt[1],[Tile::Room,Tile::Door]
        warnBad[1] += 1
      end

      #Warn?
      for i in 1..4
        test = [currPt[0],currPt[1]]
        test[0] += 1 if i==1
        test[0] -= 1 if i==2
        test[1] += 1 if i==3
        test[1] -= 1 if i==4
        next unless @map.valid_tile? test[0] , test[1]
        next if okPts.include? test
        warnBad[0] += 1 if @map.is_tile? test[0],test[1],[Tile::Room,Tile::Door]
      end

      #Continue building the path.
      path.push [currPt[0],currPt[1]]
      return false
    end


    #NOTE: This generates "bridges", which are rickity and not simply straight lines. 
    #      For connecting the remaining rooms, we might want to generate straighter lines 
    #      (since they represent tunnels).
    def generate_tri_paths(rmA, rmB, rmC, defaultCenter)
      #The center (cent) is guaranteed to be in a sea of blues (42) and special-reds (Tile::Bridge).
      #These may come from overlapping rectangles; i.e., they need to be searched.
      #Our first order of business is to get a list of all red tiles.
      red_tiles = {}  #[x,y]=>1
      to_search = [defaultCenter]
      blacklist = {} #Don't search these twice. [x,y]=>1
      red_rect = nil  #RoomGroup of min/max X and Y
      while !to_search.empty?
        #Get and add the next tile.
        next_tile = to_search.shift
        if @map.is_tile? next_tile[0],next_tile[1],Tile::Bridge
          red_tiles[next_tile] = 1
          red_rect = RoomGroup.new next_tile[0], next_tile[1], next_tile[0], next_tile[1] unless red_rect
          red_rect.lx = [red_rect.lx, next_tile[0]].min
          red_rect.ly = [red_rect.ly, next_tile[1]].min
          red_rect.hx = [red_rect.hx, next_tile[0]].max
          red_rect.hy = [red_rect.hy, next_tile[1]].max
        end
        blacklist[next_tile] = 1

        #Add its neighbors to the search list.
        orig_tile = next_tile
        for i in 1..4
          #Where to search next.
          next_tile = [orig_tile[0], orig_tile[1]]
          next_tile[0] += 1 if i==1
          next_tile[0] -= 1 if i==2
          next_tile[1] += 1 if i==3
          next_tile[1] -= 1 if i==4
          next if blacklist.include?(next_tile) || to_search.include?(next_tile)
          next unless @map.valid_tile? next_tile[0],next_tile[1]
          to_search.push(next_tile) if @map.is_tile? next_tile[0],next_tile[1],[Tile::Bridge,Tile::Pit]
        end
      end

      #In the event that NO tiles are red, color the defaultCenter and count it.
      if red_tiles.empty?
        @map.data[defaultCenter[0]][defaultCenter[1]][0] = Tile::Bridge
        red_tiles[defaultCenter] = 1
      end

      #We grow to each room, randomly, in order.
      for rm in shuffle([rmA, rmB, rmC])
        #Build a list of candidates
        candidates = {}  #N/S/E/W->[StartX,StartY,EndX,EndY,Dist] EndX,EndY is a red square; startX/Y is randomized over the leading edge.
        min_dist = nil
        for d in ["N","S","E","W"]
          #Build startX,startY
          candidates[d] = [0,0,0,0,nil]
          if d=="E" || d=="W"
            #startX is fixed; pick a random startY
            candidates[d][0] = (d=="E" ? rm.hx : rm.lx)
            ends = [rm.ly, rm.hy]
            if ends[1]-ends[0]>=2
              ends[0] += 1
              ends[1] -= 1
            end
            candidates[d][1] = uniform(ends[0], ends[1])
          else
            #startY is fixed; pick a random startX
            candidates[d][1] = (d=="S" ? rm.hy : rm.ly)
            ends = [rm.lx, rm.hx]
            if ends[1]-ends[0]>=2
              ends[0] += 1
              ends[1] -= 1
            end
            candidates[d][0] = uniform(ends[0], ends[1])
          end
        end

        #Now, for each candidate, pick the closest end point from the pool of red tiles.
        for dir,cand in candidates
          for red,v in red_tiles
            #We use Manhatten distance, since we'll be building straight lines anyway
            newD = (cand[0]-red[0]).abs + (cand[1]-red[1]).abs
            if cand[4].nil? || newD<cand[4]  #TODO: This favors the first match
              cand[2] = red[0]
              cand[3] = red[1]
              cand[4] = newD
              min_dist = newD if (min_dist.nil? || newD<min_dist)
            end
          end
        end

        #We now weight the candidates. Any candidate within 2 tiles of the minimum distance counts.
        old_candidates = candidates
        candidates = {}
        for dir,cand in old_candidates
          if cand[4]-min_dist <= 2
            candidates[dir] = cand
          end
        end

        #Finally, just select one.
        candDir = sample(candidates.keys)
        cand = candidates[candDir]
        candStart = [cand[0],cand[1]]
        candEnd = [cand[2],cand[3]]

        #Spot the original tile a different color
        @map.data[candStart[0]][candStart[1]][0] = Tile::Door

        #Spot and save 1 (+1) tiles "out" (in the same direction) --we know we have at least 1 tile between rooms.
        if candDir=="E"
          candStart[0] += 1 if candStart[0]+1<=@map.width
        elsif candDir=="W"
          candStart[0] -= 1 if candStart[0]-1>=0
        elsif candDir=="S"
          candStart[1] += 1 if candStart[1]+1<=@map.height
        elsif candDir=="N"
          candStart[1] -= 1 if candStart[1]-1>=0
        end
        @map.data[candStart[0]][candStart[1]][0] = Tile::Bridge
        red_tiles[candStart] = 1
        if candDir=="E"
          candStart[0] += 1 if (candStart[0]+1<=@map.width) && (!@map.is_tile? candStart[0]+1,candStart[1],Tile::Room)
        elsif candDir=="W"
          candStart[0] -= 1 if (candStart[0]-1>=0) && (!@map.is_tile? candStart[0]-1,candStart[1],Tile::Room)
        elsif candDir=="S"
          candStart[1] += 1 if (candStart[1]+1<=@map.height) && (!@map.is_tile? candStart[0],candStart[1]+1,Tile::Room)
        elsif candDir=="N"
          candStart[1] -= 1 if (candStart[1]-1>=0) && (!@map.is_tile? candStart[0],candStart[1]-1,Tile::Room)
        end
        @map.data[candStart[0]][candStart[1]][0] = Tile::Bridge
        red_tiles[candStart] = 1

        #We now start building outward. We build from the start and end in turn, until they meet.
        growDir = 0 #1=from,to, 0=to,from
        last_grows = [nil,nil] #Which direction we were growing last.
        for temp in 1..100 #Can't draw forever...
          break if (candStart[0]==candEnd[0]) && (candStart[1]==candEnd[1])

          #Grow in which direction?
          growDir = (growDir+1)%2
          fromT,toT = candStart,candEnd
          if growDir==0
            fromT,toT = toT,fromT
          end

          #With 90% chance, keep growing in the previous direction.
          currGrow = last_grows[growDir]
          currGrow = nil if @@my_rand.rand>=0.9

          #Also reset (to nil) if we MUST turn.
          if !currGrow.nil?
            if (currGrow=="E" || currGrow=="W") && fromT[1]==toT[1]
              currGrow = nil
            end
            if (currGrow=="N" || currGrow=="S") && fromT[0]==toT[0]
              currGrow = nil
            end
          end

          #We also reset if we can't move in that direction (if it's a black area)
          if !currGrow.nil?
            if currGrow=="E" && (fromT[0]+1>@map.width || @map.is_tile?(fromT[0]+1,fromT[1],[Tile::Room,Tile::Bridge]))
              currGrow = nil
            end
            if currGrow=="W" && (fromT[0]-1<0 || @map.is_tile?(fromT[0]-1,fromT[1],[Tile::Room,Tile::Bridge]))
              currGrow = nil
            end
            if currGrow=="S" && (fromT[1]+1>@map.height || @map.is_tile?(fromT[0],fromT[1]+1,[Tile::Room,Tile::Bridge]))
              currGrow = nil
            end
            if currGrow=="N" && (fromT[1]-1<0 || @map.is_tile?(fromT[0],fromT[1]-1,[Tile::Room,Tile::Bridge]))
              currGrow = nil
            end
          end

          #Now, determine if we need a new growing direction.
          if currGrow.nil?
            wallBad = 1 #Added to a dir when a wall is +2 tiles (+1 removes it entirely)
            options = {} #dir=>BadVal; lower is more desirable.

            #We have special cases for when one of the offsets is 0 (they never both are; the while condition checks that)
            if fromT[0]==toT[0]
              #Points line up vertically.
              if toT[1]>fromT[1]
                options = {"S"=>1, "E"=>2, "W"=>2, "N"=>3}
              else
                options = {"N"=>1, "E"=>2, "W"=>2, "S"=>3}
              end
            elsif fromT[1]==toT[1]
              #Points line up horizontally.
              if toT[0]>fromT[0]
                options = {"E"=>1, "N"=>2, "S"=>2, "W"=>3}
              else
                options = {"W"=>1, "N"=>2, "S"=>2, "E"=>3}
              end
            else
              #Our "preferred" direction is still the one with the greater distance, but we have a "second preferred" now which is orthogonal.
              distX = (toT[0]-fromT[0]).abs
              distY = (toT[1]-fromT[1]).abs
              if distY >= distX  #TODO: We are arbitrarily depressing a direction if it's the same weight.
                #Go vertical first.
                if toT[1]>fromT[1]
                  options = {"S"=>1, "N"=>3}
                else
                  options = {"N"=>1, "S"=>3}
                end
                if toT[0]>fromT[0]
                  options["E"] = 2
                  options["W"] = 3
                else
                  options["W"] = 2
                  options["E"] = 3
                end
              else
                #Go horizontal first.
                if toT[0]>fromT[0]
                  options = {"E"=>1, "W"=>3}
                else
                  options = {"W"=>1, "E"=>3}
                end
                if toT[1]>fromT[1]
                  options["S"] = 2
                  options["N"] = 3
                else
                  options["N"] = 2
                  options["S"] = 3
                end
              end
            end

            #This really should never happen...
            if options.empty?
              puts "ERROR: options empty when growing..."
              break
            end

            #Now, we remove ones that can't possibly work.
            #NOTE: We don't remove an option which is red 1 tile away (because we may need to backtrack).
            for dir in ["N","S","E","W"]
              #Blacklist
              if dir=="E" && (fromT[0]+1>@map.width || @map.is_tile?(fromT[0]+1,fromT[1],[Tile::Room,Tile::Door]))
                options[dir] = nil
              end
              if dir=="W" && (fromT[0]-1<0 || @map.is_tile?(fromT[0]-1,fromT[1],[Tile::Room,Tile::Door]))
                options[dir] = nil
              end
              if dir=="S" && (fromT[1]+1>@map.height || @map.is_tile?(fromT[0],fromT[1]+1,[Tile::Room,Tile::Door]))
                options[dir] = nil
              end
              if dir=="N" && (fromT[1]-1<0 || @map.is_tile?(fromT[0],fromT[1]-1,[Tile::Room,Tile::Door]))
                options[dir] = nil
              end
              #Depreference
              if dir=="E" && (fromT[0]+2>@map.width || @map.is_tile?(fromT[0]+2,fromT[1],[Tile::Room,Tile::Bridge,Tile::Door]))
                options[dir] += wallBad unless options[dir].nil?
              end
              if dir=="W" && (fromT[0]-2<0 || @map.is_tile?(fromT[0]-2,fromT[1],[Tile::Room,Tile::Bridge,Tile::Door]))
                options[dir] += wallBad unless options[dir].nil?
              end
              if dir=="S" && (fromT[1]+2>@map.height || @map.is_tile?(fromT[0],fromT[1]+2,[Tile::Room,Tile::Bridge,Tile::Door]))
                options[dir] += wallBad unless options[dir].nil?
              end
              if dir=="N" && (fromT[1]-2<0 || @map.is_tile?(fromT[0],fromT[1]-2,[Tile::Room,Tile::Bridge,Tile::Door]))
                options[dir] += wallBad unless options[dir].nil?
              end
            end

            #Now, pick one.
            minItems = []
            for dir in ["N","S","E","W"]
              next if options[dir].nil?
              if minItems.empty?
                minItems.push [dir,options[dir]]
              else
                if options[dir]<=minItems[0][1]
                  minItems = [] if options[dir]<minItems[0][1]
                  minItems.push [dir,options[dir]]
                end
              end
            end

            currGrow = sample(minItems)[0] unless minItems.empty? #We warn on empty later.
          end

          #At this point we MUST have a growing direction. Color it and update.
          if currGrow.nil?
            puts "ERROR: Can't grow outward?"
            break
          end
          if currGrow=="E"
            fromT[0] += 1
          end
          if currGrow=="W"
            fromT[0] -= 1
          end
          if currGrow=="S"
            fromT[1] += 1
          end
          if currGrow=="N"
            fromT[1] -= 1
          end
          @map.data[fromT[0]][fromT[1]][0] = Tile::Bridge
          red_tiles[fromT] = 1
        end
      end
    end


    #Perform some generic cleanup
    def cleanup_rooms()
      #See if any red tiles are inside the bounds of a room
      @room_groups.each {|rect|
        rect.rooms.each {|rm|
          for x in rm.lx..rm.hx
            for y in rm.ly..rm.hy
              if @map.is_tile? x,y,[Tile::Bridge,Tile::Tunnel]
                replace = "black"
                if (x==rm.lx || x==rm.hx) && y>=rm.ly && y<=rm.hy
                  replace = "pink"
                elsif (y==rm.ly || y==rm.hy) && x>=rm.lx && x<=rm.hx
                  replace = "pink"
                end
                puts "WARNING: path inside room! Replacing with #{replace}"
                @map.data[x][y][0] = (replace=="black" ? Tile::Room : Tile::Door)
              end
            end
          end
        }
      }
    end


    #Does a connection (a->b or b->a) exist?
    def connects?(rmA, rmB)
      for pair in @connections
        return true if pair[0]==rmA && pair[1]==rmB
        return true if pair[0]==rmB && pair[1]==rmA
      end
      return false
    end

    #Does bi-directional link (a->b or b->a) exist?
    def in_bidilist?(bidilist, rmA, rmB)
      for pair in bidilist
        return true if pair[0]==rmA && pair[1]==rmB
        return true if pair[0]==rmB && pair[1]==rmA
      end
      return false
    end

    #Does tri-directional link (a->b->c or b->a->c, etc.) exist?
    def in_tridilist?(tridilist, rmA, rmB, rmC)
      for pair in tridilist
        return true if pair[0]==rmA && pair[1]==rmB && pair[2]==rmC
        return true if pair[0]==rmA && pair[1]==rmC && pair[2]==rmB
        return true if pair[0]==rmB && pair[1]==rmA && pair[2]==rmC
        return true if pair[0]==rmB && pair[1]==rmC && pair[2]==rmA
        return true if pair[0]==rmC && pair[1]==rmA && pair[2]==rmB
        return true if pair[0]==rmC && pair[1]==rmB && pair[2]==rmA
      end
      return false
    end

    #Compute angles of a triangle from 3 rooms' centers (also computes the triangle's center)
    def compute_triangle(rmA, rmB, rmC)
      #Get the 3 centers.
      rmA = [rmA.lx + (rmA.hx-rmA.lx)/2 , rmA.ly + (rmA.hy-rmA.ly)/2]
      rmB = [rmB.lx + (rmB.hx-rmB.lx)/2 , rmB.ly + (rmB.hy-rmB.ly)/2]
      rmC = [rmC.lx + (rmC.hx-rmC.lx)/2 , rmC.ly + (rmC.hy-rmC.ly)/2]

      #It is possible these are not a triangle!
      return nil if rmA[0]==rmB[0] && rmA[0]==rmC[0]
      return nil if rmA[1]==rmB[1] && rmA[1]==rmC[1]

      #...and:
      return nil if rmA[0] * (rmB[1] - rmC[1]) + rmB[0] * (rmC[1] - rmA[1]) + rmC[0] * (rmA[1] - rmB[1]) == 0

      #Compute the 3 distances
      dA = dist(rmB, rmC)
      dB = dist(rmA, rmC)
      dC = dist(rmA, rmB)

      #Get the largest angle (which is opposite the largest side). Put it in B (for no reason)
      if dA>dB && dA>dC
        dB,dA = dA,dB
      elsif dC>dB && dC>dA
        dB,dC = dC,dB
      end

      #NOTE: We could also check that the internal calculation is within EPSILON of -1.0 or 1.0 (but the area check also works)
      angB = Math.acos ((dA**2 + dC**2 - dB**2)/(2*dA*dC))

      #Get the 2 remaining angles.
      angC = Math.asin (dC * Math.sin(angB) / dB)
      angA = Math::PI - angB - angC

      #Convert to degrees
      angA *= (180/Math::PI)
      angB *= (180/Math::PI)
      angC *= (180/Math::PI)
    
      #Compute the center.
      centX = (rmA[0]+rmB[0]+rmC[0])/3
      centY = (rmA[1]+rmB[1]+rmC[1])/3

      #Return it all
      return Triangle.new centX, centY, angA, angB, angC
    end

    def dist(pt1, pt2)
      dx = pt2[0] - pt1[0]
      dy = pt2[1] - pt1[1]
      return Math.sqrt(dx**2 + dy**2)
    end

  end

  # Sample test driver.
  def self.main()
    iDEAL_ROOM = [15,12]       # 18x13 fits on screen (RPG Maker). We want roughly 75% of that.
    iDEAL_DUNGEON = [80,50]    # What a "normal" sized dungeon room looks like.
    mAX_DUNGEON   = [150,150]  # The absolute maximum size for a dungeon (defined by your game map, usually).

    temp = Fancy_Map.new(iDEAL_ROOM, iDEAL_DUNGEON, mAX_DUNGEON)
    temp.setup()
    temp.pretty_print "canvas.png"

    puts "Done"
  end


end 


RandomDungeon::main if __FILE__==$0


