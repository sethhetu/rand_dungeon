Thanks for downloading Trump 3016! To play the game:

On Windows, double-click "Game_Windows.exe"
On Linux, go into a terminal and type "./Game_Linux"
On OS-X, right-click on Trump3016_OSX and choose "Open..."

Controls:
  Arrow keys to move
  Z/X for Accept/Cancel (X is also "Menu")
  Shift to run
  Q/W for pageup/pagedown (in menus)
  F6 to change window size.

The game is fairly broken past floor 10. My apologies!

Oh, and once you're done judging, talk to the guy in the suit to get a "Fabric of Reality" early.
Post your favorite random seed online!

